TreoSMS message exporting tool
==============================
v1.4 (2017)

Simple console tool extracting SMS plain text data from Palm Treo 680 device database.

SYSTEM REQUIREMENTS
-------------------

Installed Oracle Java Runtime Environment - version at least 1.7
https://java.com/download/

SUPPORTED DEVICES
-----------------

* Palm Treo 680
* any other Palm SMS database not tested

USAGE
-----

Locate Messages_Database.PDB file in your Palm backup data (eg. C:\Palm\VMTREO\Backup\Messages_Database.PDB) and use it
as input for this tool:

java -jar "TreoSMS.jar" -z +1 -t -i Messages_Database.PDB -o output.csv

Output (output.csv) can be opened in any text editor or Micro$oft Excel.

KNOWN ISSUES
------------

There is some timezone or daylight saving time (DST) related issue, so that I'm not sure about the parsed time of the messages.
Looks like Palm is storing the time with no timezone or DST information and there is some 1 hour offset error sometimes.

There comes the -z switch to be useful.

To be clear - TreoSMS uses Java local timezone and DST aware date-time formatter.


LICENSE
-------
Licensed under the [GPL](http://www.gnu.org/licenses/gpl.html).

A few extracted classes from jSyncManager API (LGPL) used due to errors (unresolved dependencies) in the published library (http://jsyncmanager.sourceforge.net/).


CHANGELOG
---------

v1.4 (2017-01-03): malformed SMS record check, timezone and DST related unit tests and code clean up
v1.3 ()
v1.2
v1.1
v1.0
