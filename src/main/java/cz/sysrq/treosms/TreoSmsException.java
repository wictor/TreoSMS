/*
 * Project: TreoSMS 2008-01-22 - https://gitlab.com/wictor/TreoSMS
 * Author: wictor@sysrq.cz
 * Licensed under the [GPL](http://www.gnu.org/licenses/gpl.html).
 */

package cz.sysrq.treosms;

/**
 * Base exception class.
 *
 * @author wictor
 */
public class TreoSmsException extends Exception {

    public TreoSmsException() {
    }

    public TreoSmsException(String message) {
        super(message);
    }

    public TreoSmsException(String message, Throwable cause) {
        super(message, cause);
    }

    public TreoSmsException(Throwable cause) {
        super(cause);
    }

    public TreoSmsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
