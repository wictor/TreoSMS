/*
 * Project: TreoSMS 2008-01-22 - https://gitlab.com/wictor/TreoSMS
 * Author: wictor@sysrq.cz
 * Licensed under the [GPL](http://www.gnu.org/licenses/gpl.html).
 */

package cz.sysrq.treosms.format;

import cz.sysrq.treosms.data.Message;

/**
 * Message formating processor interface.
 * <p/>
 * Created by wictor on 4.8.2015.
 */
public interface MessageFormatter {

    /**
     * Processes message to formatted string.
     *
     * @param msg message
     * @return formatted string
     */
    String format(Message msg);
}
