/*
 * Project: TreoSMS 2008-01-22 - https://gitlab.com/wictor/TreoSMS
 * Author: wictor@sysrq.cz
 * Licensed under the [GPL](http://www.gnu.org/licenses/gpl.html).
 */

package cz.sysrq.treosms.format;

import cz.sysrq.treosms.data.Message;
import cz.sysrq.treosms.data.SmsDateType;

/**
 * Messages processor to summary format.
 *
 * @author wictor
 */
public class SummaryFormatter implements MessageFormatter {

    /**
     * difference in seconds between GMT and current timezone
     */
    private final int timeZoneDiff;

    /**
     * Constructor with time diff parameter to adjust the Palm "unzoned" timestamp.
     *
     * @param timeZoneDiff difference in seconds between GMT and current timezone
     */
    public SummaryFormatter(int timeZoneDiff) {
        this.timeZoneDiff = timeZoneDiff;
    }

    public String format(Message msg) {
        String dir = msg.getDirection().name().substring(0, 1);
        String name = msg.getParty().getName();
        String numb = msg.getParty().getNumber();
        String date = msg.getDatetime(SmsDateType.T8000, timeZoneDiff);
        String dateRaw = msg.getDates().get(0).getDateValue().toString();
        String text = msg.getText().replace((char) 0x0a, ' ').replace("\"", "\\\"");
        if (text.length() > 18) {
            text = text.substring(0, 10) + "..." + text.substring(text.length() - 5, text.length());
        }

        return String.format("%s %s <%s> %s (%s) \"%s\" (%d)",
                dir, name, numb, date, dateRaw, text, msg.getText().length());
    }

}
