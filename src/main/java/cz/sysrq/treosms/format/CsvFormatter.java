/*
 * Project: TreoSMS 2008-01-22 - https://gitlab.com/wictor/TreoSMS
 * Author: wictor@sysrq.cz
 * Licensed under the [GPL](http://www.gnu.org/licenses/gpl.html).
 */

package cz.sysrq.treosms.format;

import cz.sysrq.treosms.data.Message;
import cz.sysrq.treosms.data.SmsDateType;

/**
 * Messages processor to CSV format.
 *
 * @author wictor
 */
public class CsvFormatter implements MessageFormatter {

    /**
     * difference in seconds between GMT and current timezone
     */
    private final int timeZoneDiff;

    /**
     * Constructor with time diff parameter to adjust the Palm "unzoned" timestamp.
     *
     * @param timeZoneDiff difference in seconds between GMT and current timezone
     */
    public CsvFormatter(int timeZoneDiff) {
        this.timeZoneDiff = timeZoneDiff;
    }

    public String format(Message msg) {
        String name = msg.getParty().getName();
        String numb = msg.getParty().getNumber();
        String date = msg.getDatetime(SmsDateType.T8000, timeZoneDiff);

        /*
         * Fields with embedded double-quote characters must be delimited
         * with double-quote characters, and the embedded double-quote
         * characters must be represented by a pair of double-quote
         * characters.
         *
         * Remember, that you have to be sure, that all the double-quotes
         * are in double-quoted fields.
         */
        String text = msg.getText().replace("\"", "\"\"");

        return String.format("%s,\"%s\",%s,%s,\"%s\"",
                msg.getDirection(), name, numb, date, text);
    }

}
