/*
 * Project: TreoSMS 2008-01-22 - https://gitlab.com/wictor/TreoSMS
 * Author: wictor@sysrq.cz
 * Licensed under the [GPL](http://www.gnu.org/licenses/gpl.html).
 */

package cz.sysrq.treosms.parser;

import cz.sysrq.treosms.data.Message;
import cz.sysrq.treosms.data.MessageParsingException;
import cz.sysrq.treosms.data.ParsingResult;
import org.jSyncManager.API.Protocol.Util.DLPDatabase;
import org.jSyncManager.API.Protocol.Util.DLPRecord;

/**
 * Message Database record parser common interface.
 *
 * @author wictor
 * @since 27.7.2015.
 */
public interface MessageParser {

    /**
     * Main entry-point to parse the whole Message Database.
     * Unknown formatted records are collected to the result as well without interrupting the whole process.
     *
     * @param database Message Database
     * @return parsing result containing identified messages and unknown records
     */
    ParsingResult parseDb(DLPDatabase database);

    /**
     * Single record parsing.
     *
     * @param record Message Database record
     * @return identified message data
     * @throws MessageParsingException when unknown record format found
     */
    Message parseRecord(DLPRecord record) throws MessageParsingException;
}
