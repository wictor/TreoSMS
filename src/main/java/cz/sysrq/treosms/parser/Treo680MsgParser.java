/*
 * Project: TreoSMS 2008-01-22 - https://gitlab.com/wictor/TreoSMS
 * Author: wictor@sysrq.cz
 * Licensed under the [GPL](http://www.gnu.org/licenses/gpl.html).
 */

package cz.sysrq.treosms.parser;

import cz.sysrq.treosms.data.Dir;
import cz.sysrq.treosms.data.FailedRecord;
import cz.sysrq.treosms.data.Message;
import cz.sysrq.treosms.data.MessageParsingException;
import cz.sysrq.treosms.data.ParsingResult;
import cz.sysrq.treosms.data.Party;
import cz.sysrq.treosms.data.SmsDate;
import cz.sysrq.treosms.data.SmsDateType;
import org.jSyncManager.API.Protocol.Util.DLPDatabase;
import org.jSyncManager.API.Protocol.Util.DLPRecord;
import org.joou.UInteger;
import org.joou.UShort;

import java.io.UnsupportedEncodingException;
import java.util.EnumSet;
import java.util.LinkedList;
import java.util.List;

/**
 * Message parser for PALM TREO 680 Message Database records format.
 *
 * @author wictor
 */
public class Treo680MsgParser implements MessageParser {

    /**
     * Unknown byte sequence
     */
    private static final byte[] SEQUENCE_000003 = {0, 0, 0, 0, 0, 3};
    /**
     * TRSM byte sequence
     */
    private static final byte[] SEQUENCE_TRSM = {0, 0x1b, 0x54, 0x72, 0x73, 0x6d};

    @Override
    public ParsingResult parseDb(DLPDatabase database) {
        List<Message> messages = new LinkedList<>();
        List<FailedRecord> otherRecords = new LinkedList<>();
        List<FailedRecord> failedRecords = new LinkedList<>();

        for (int i = 0; i < database.getElements(); i++) {
            DLPRecord rec = (DLPRecord) database.getElement(i);

            try {
                Message message = parseRecord(rec);
                if (message != null) {
                    messages.add(message);
                } else {
                    otherRecords.add(new FailedRecord(rec, new MessageParsingException("NotSmsRecordException")));
                }
            } catch (MessageParsingException e) {
                failedRecords.add(new FailedRecord(rec, e));
            }
        }

        return new ParsingResult(messages, otherRecords, failedRecords);
    }

    @Override
    public Message parseRecord(DLPRecord record) throws MessageParsingException {
        BufferReader reader = new BufferReader(record.getData());
        // parse header -> 10 or 16 bytes
        MsgHeader hdr = MsgHeader.decode(reader);
        reader.skipN(hdr.getHdrLen());
        if (!hdr.isSmsType()) {
            return null;
        }

        // parse body -> while not finished: check code, read code block
        List<SmsDate> dates = new LinkedList<>();
        Party party = null;
        Dir direction = null;
        String text = null;

        while(!reader.finished()) {
            // handle special cases
            if (reader.containsHere(SEQUENCE_000003)) {
                reader.skipN(SEQUENCE_000003.length);
                continue;
            }
            if (reader.containsHere(SEQUENCE_TRSM)) {
                reader.skipN(SEQUENCE_TRSM.length);
                continue;
            }
            // handle standard cases
            UShort code = reader.read2();
            CtrlCode ctrlCode = CtrlCode.byCode(code);
            switch (ctrlCode) {
                case DATE1:
                case DATE2:
                case DATE3:
                case DATE4:
                    dates.add(new SmsDate(reader.read4(), ctrlCode.code));
                    break;
                case DST_PARTY:
                    party = parseParty(reader.readParty());
                    direction = Dir.SENT;
                    break;
                case SRC_PARTY:
                    party = parseParty(reader.readParty());
                    direction = Dir.RECEIVED;
                    break;
                case TEXT:
                    text = reader.readString();
                    break;
                case LINK:
                case ERR_MSG:
                case PADDING:
                    int len = reader.read2().intValue();
                    reader.skipN(len);
                default:
                    reader.skipN(ctrlCode.dataLen);
            }
        }
        if (dates.isEmpty() || party == null || direction == null || text == null) {
            throw new MessageParsingException("MalformedRecord");
        }
        return new Message(dates, party, direction, text);
    }

    private Party parseParty(String[] party) {
        if (party.length == 1) {
            return new Party(party[0]);
        } else {
            return new Party(party[0], party[1]);
        }
    }

    /**
     * Buffer reader wrapper handling cursor movement.
     */
    private static class BufferReader {

        private final byte[] buffer;
        private int pos;

        public BufferReader(byte[] buffer) {
            this.buffer = buffer;
            this.pos = 0;
        }

        public UShort read2() {
            int d = (buffer[pos] & 0xff) * 0x100 + (buffer[pos + 1] & 0xff);
            pos += 2;
            return UShort.valueOf(d);
        }

        public UInteger read4() {
            long d = (buffer[pos] & 0xff) * 0x1000000L + (buffer[pos + 1] & 0xff) * 0x10000L + (buffer[pos + 2] & 0xff) * 0x100L + (buffer[pos + 3] & 0xff);
            pos += 4;
            return UInteger.valueOf(d);
        }

        public String readString() {
            int length = read2().intValue();
            try {
                String s = new String(buffer, pos, length - 1, "ISO-8859-1");
                pos += length;
                return s;
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException("Unsupported encoding");
            }
        }

        public String[] readParty() {
            // partyStrRaw is some garbage covered string containing number and name in the middle
            String partyStrRaw = readString();
            // looks like it is always 20 bytes of 0x00 and some strange variable byte (even printable char) just before the number,
            // so we have to strip it by position
            String partyWithoutPrefixingMess = partyStrRaw.substring(20);
            String stripped = partyWithoutPrefixingMess.replaceAll("\\p{C}", " ").trim();
            return stripped.split(" ", 2);
        }

        public void skipN(int n) {
            pos += n;
        }

        public boolean containsHere(byte[] arr) {
            for (int i = 0; i < arr.length; i++) {
                if (buffer[pos + i] != arr[i]) {
                    return false;
                }
            }
            return true;
        }

        public boolean finished() {
            return pos == buffer.length;
        }

        public String getBytesString(int length) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < length; i++) {
                String str = String.format("%1$02x ", buffer[pos + i]);
                sb.append(str);
            }
            sb.deleteCharAt(sb.length() - 1);
            return sb.toString();
        }

        @Override
        public String toString() {
            return "BufferReader{" +
                    "pos=" + pos +
                    (pos < buffer.length -2 ? String.format(" next code=%1$02x %2$02x", buffer[pos], buffer[pos + 1]) : " end") +
                    '}';
        }
    }

    /**
     * Control codes enumeration.
     * It looks like a record is consisting of random-ordered sequence of fields starting with a 2-bytes control code.
     * Typically field is numeric (4 bytes) or null-terminated string prefixed by length definition.
     */
    private enum CtrlCode {
        TEXT(0x2002, 0),
        SRC_PARTY(0x400C, 0),
        DST_PARTY(0x4009, 0),
        DATE1(SmsDateType.T8000.getCode().intValue(), 4),
        DATE2(SmsDateType.T0005.getCode().intValue(), 4),
        DATE3(SmsDateType.T0006.getCode().intValue(), 4),
        DATE4(SmsDateType.T0007.getCode().intValue(), 4),
        LINK(0x2005, 0),
        ERR_MSG(0x200e, 0),
        PADDING(0x4000, 0),

        CD_0002(0x0002, 4),
        CD_0003(0x0003, 4),
        CD_0008(0x0008, 4),
        CD_0014(0x0014, 4),
        CD_000D(0x000d, 4),
        CD_000E(0x000e, 4),
        CD_000F(0x000f, 4),
        ;

        private UShort code;
        private int dataLen;

        CtrlCode(int code, int dataLen) {
            this.code = UShort.valueOf(code);
            this.dataLen = dataLen;
        }

        static CtrlCode byCode(UShort code) throws MessageParsingException {
            for (CtrlCode cd : CtrlCode.values()) {
                if (cd.code.equals(code)) {
                    return cd;
                }
            }
            throw new MessageParsingException("No CtrlCode for code " + String.format("%1$02x %2$02x", (code.intValue() & 0xff00) / 0x100, code.intValue() & 0xff));
        }
    }

    /**
     * Record data heading byte sequence is some strange data type mess.
     * Looks like it distinguish records containing SMS, MMS or something else...
     */
    private enum MsgHeader {
        SMS1(new byte[] {0,0,0,0, 0,1,0,0, 0,2}),
        SMS2(new byte[] {0,0,0,0, 0,0,0,0, 0,3,0,1, 0,0,0,2}),
        // record without any message - can be call or mms or sth
        OTHER0(new byte[] {0,0,0,0, 0,1,0,0, 0,0}),
        OTHER1(new byte[] {0,0,0,0, 0,1,0,0, 0,1}),
        ;

        private byte[] data;

        MsgHeader(byte[] data) {
            this.data = data;
        }

        int getHdrLen() {
            return data.length;
        }

        boolean isSmsType() {
            return SMS_TYPES.contains(this);
        }

        private static final EnumSet<MsgHeader> SMS_TYPES = EnumSet.of(MsgHeader.SMS1, MsgHeader.SMS2);

        static MsgHeader decode(BufferReader reader) throws MessageParsingException {
            for (MsgHeader mh : MsgHeader.values()) {
                if (reader.containsHere(mh.data)) {
                    return mh;
                }
            }
            throw new MessageParsingException("Unknown header: " + reader.getBytesString(16));
        }
    }
}
