/*
 * Project: TreoSMS 2008-01-22 - https://gitlab.com/wictor/TreoSMS
 * Author: wictor@sysrq.cz
 * Licensed under the [GPL](http://www.gnu.org/licenses/gpl.html).
 */

package cz.sysrq.treosms.data;

/**
 * Message source or target party.
 *
 * @author wictor
 */
public class Party {

    private final String number;
    private final String name;

    public Party(String number, String name) {
        this.number = number;
        this.name = name;
    }

    public Party(String number) {
        this(number, null);
    }

    public String getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Party{" +
                "number='" + number + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
