/*
 * Project: TreoSMS 2008-01-22 - https://gitlab.com/wictor/TreoSMS
 * Author: wictor@sysrq.cz
 * Licensed under the [GPL](http://www.gnu.org/licenses/gpl.html).
 */

package cz.sysrq.treosms.data;

import org.jSyncManager.API.Protocol.Util.DLPRecord;

/**
 * Parsing failed record container. Holding the record + parsing error detail.
 *
 * @author wictor
 */
public class FailedRecord {

    private final DLPRecord record;

    private final MessageParsingException ex;

    public FailedRecord(DLPRecord record, MessageParsingException ex) {
        this.record = record;
        this.ex = ex;
    }

    public DLPRecord getRecord() {
        return record;
    }

    public MessageParsingException getEx() {
        return ex;
    }

    @Override
    public String toString() {
        return "FailedRecord:" +
                "failing reason=" + ex +
                "\n" + record;
    }
}
