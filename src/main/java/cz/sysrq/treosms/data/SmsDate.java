/*
 * Project: TreoSMS 2008-01-22 - https://gitlab.com/wictor/TreoSMS
 * Author: wictor@sysrq.cz
 * Licensed under the [GPL](http://www.gnu.org/licenses/gpl.html).
 */

package cz.sysrq.treosms.data;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.joou.UInteger;
import org.joou.UShort;

/**
 * SMS date = type + date value.
 *
 * @author wictor
 */
public class SmsDate {

    /**
     * difference in seconds between Palm timebase of 12 AM Jan 1, 1904, and UNIX timebase of 12 AM Jan 1, 1970
     */
    private static final int PALM_UNIX_TIMEBASE_DIFFERENCE = 2082848400;

    private final UInteger dateValue;

    private final SmsDateType dateType;

    public SmsDate(long dateValue, SmsDateType dateType) {
        this.dateValue = UInteger.valueOf(dateValue);
        this.dateType = dateType;
    }

    public SmsDate(UInteger dateValue, UShort dateType) {
        this.dateValue = dateValue;
        this.dateType = SmsDateType.byCode(dateType);
    }

    /**
     * Returns the formatted string of message date/time value.
     *
     * @param zoneDiff difference in seconds between GMT and current timezone.
     * @param format a formatting String - see <code>SimpleDateFormat</code>.
     * @param tz explicit timezone used for formatting (null = default local zone)
     * @return the formatted string of message datetime value.
     */
    public String getFormattedDate(int zoneDiff, String format, TimeZone tz) {
        long dt = (dateValue.longValue() - PALM_UNIX_TIMEBASE_DIFFERENCE + zoneDiff) * 1000;
        Date d = new Date(dt);
        SimpleDateFormat f = new SimpleDateFormat(format);
        if (tz != null) {
            f.setTimeZone(tz);
        }
        return f.format(d);
    }

    /**
     * Returns the formatted string of message date/time value.
     *
     * @param zoneDiff difference in seconds between GMT and current timezone.
     * @param format a formatting String - see <code>SimpleDateFormat</code>.
     * @return the formatted string of message datetime value.
     */
    public String getFormattedDate(int zoneDiff, String format) {
        return getFormattedDate(zoneDiff, format, null);
    }

    public String getFormattedDate(int zoneDiff) {
        return getFormattedDate(zoneDiff, "yyyy-MM-dd HH:mm:ss");
    }

    public UInteger getDateValue() {
        return dateValue;
    }

    public SmsDateType getDateType() {
        return dateType;
    }

    @Override
    public String toString() {
        return "SmsDate{" +
                "dateType=" + dateType +
                ", dateValue=" + getFormattedDate(0) +
                '}';
    }
}
