/*
 * Project: TreoSMS 2008-01-22 - https://gitlab.com/wictor/TreoSMS
 * Author: wictor@sysrq.cz
 * Licensed under the [GPL](http://www.gnu.org/licenses/gpl.html).
 */

package cz.sysrq.treosms.data;

import java.util.List;
import java.util.Objects;

/**
 * A message bean class.
 *
 * @author wictor
 */
public class Message {
    
    /**
     * All 3 timestamps of this message.
     */
    private final List<SmsDate> dates;
    
    /**
     * Phone number and a name of the other side (sender or receiver depending on
     * the direction of the message).
     */
    private final Party party;
    
    /**
     * The direction of this message (RECEIVED or SENT).
     */
    private final Dir direction;
    
    /**
     * The message body.
     */
    private final String text;

    public Message(List<SmsDate> dates, Party party, Dir direction, String text) {
        Objects.requireNonNull(dates);
        Objects.requireNonNull(party);
        Objects.requireNonNull(direction);
        Objects.requireNonNull(text);

        this.dates = dates;
        this.party = party;
        this.direction = direction;
        this.text = text;
    }

    public String getDatetime(SmsDateType type, int zoneDiff) {
        return getDatetimeByFormat(type, "yyyy-MM-dd HH:mm:ss", zoneDiff);
    }
    
    /**
     * Returns the formatted string of message date/time value.
     * @param type a sms date type.
     * @param format a formatting String - see <code>SimpleDateFormat</code>.
     * @param zoneDiff difference in seconds between GMT and current timezone.
     * @return the formatted string of message datetime value or {@code null} if the date type is not set.
     */
    private String getDatetimeByFormat(SmsDateType type, String format, int zoneDiff) {
        for (SmsDate date : dates) {
            if (date.getDateType().equals(type)) {
                return date.getFormattedDate(zoneDiff, format);
            }
        }
        return null;
    }
    
    public String getText() {
        return text;
    }

    public List<SmsDate> getDates() {
        return dates;
    }

    public Party getParty() {
        return party;
    }

    public Dir getDirection() {
        return direction;
    }

    @Override
    public String toString() {
        String str = String.format("%s %s %s", direction, party, dates);
        
        String msgText = text.replace((char)0x0a, ' ').replace("\"", "\\\"");
        if (msgText.length() > 18) {
            str += " \"" + msgText.substring(0, 10) + "...";
            str += msgText.substring(msgText.length() - 5, msgText.length()) + "\" ";
        } else {
            str += " \"" + msgText + "\" ";
        }
        
        return str;
    }
}
