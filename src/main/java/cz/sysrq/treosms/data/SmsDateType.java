/*
 * Project: TreoSMS 2008-01-22 - https://gitlab.com/wictor/TreoSMS
 * Author: wictor@sysrq.cz
 * Licensed under the [GPL](http://www.gnu.org/licenses/gpl.html).
 */

package cz.sysrq.treosms.data;

import org.joou.UShort;

/**
 * SMS date type enumeration.
 *
 * @author wictor
 * @since 27.7.2015.
 */
public enum SmsDateType {
    /**
     * Unknown date type coded 0x8000.
     */
    T8000(0x8000),
    /**
     * Unknown date type coded 0x0005. Used only at RECEIVED messages.
     */
    T0005(0x0005),
    /**
     * Unknown date type coded 0x0006.
     */
    T0006(0x0006),
    /**
     * Unknown date type coded 0x0006. Used only at SENT messages.
     */
    T0007(0x0007),
    ;

    private final UShort code;

    SmsDateType(int code) {
        this.code = UShort.valueOf(code);
    }

    public UShort getCode() {
        return code;
    }

    public static SmsDateType byCode(UShort code) {
        for (SmsDateType smsDateType : SmsDateType.values()) {
            if (smsDateType.code.equals(code)) {
                return smsDateType;
            }
        }
        throw new IllegalArgumentException("No SmsDateType for code + " + code);
    }
}
