/*
 * Project: TreoSMS 2008-01-22 - https://gitlab.com/wictor/TreoSMS
 * Author: wictor@sysrq.cz
 * Licensed under the [GPL](http://www.gnu.org/licenses/gpl.html).
 */

package cz.sysrq.treosms.data;

/**
 * Message direction. RECEIVED (inbound) or SENT (outbound).
 *
 * @author wictor
 * @since 27.7.2015.
 */
public enum Dir {

    RECEIVED,
    SENT;
}
