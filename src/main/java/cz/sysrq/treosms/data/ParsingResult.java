/*
 * Project: TreoSMS 2008-01-22 - https://gitlab.com/wictor/TreoSMS
 * Author: wictor@sysrq.cz
 * Licensed under the [GPL](http://www.gnu.org/licenses/gpl.html).
 */

package cz.sysrq.treosms.data;

import java.util.List;

/**
 * Parsing results container.
 *
 * @author wictor
 */
public class ParsingResult {

    private final List<Message> messages;

    private final List<FailedRecord> skippedRecords;

    private final List<FailedRecord> failedRecords;

    public ParsingResult(List<Message> messages, List<FailedRecord> skippedRecords, List<FailedRecord> failedRecords) {
        this.messages = messages;
        this.skippedRecords = skippedRecords;
        this.failedRecords = failedRecords;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public List<FailedRecord> getSkippedRecords() {
        return skippedRecords;
    }

    public List<FailedRecord> getFailedRecords() {
        return failedRecords;
    }
}
