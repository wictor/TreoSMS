/*
 * Project: TreoSMS 2008-01-22 - https://gitlab.com/wictor/TreoSMS
 * Author: wictor@sysrq.cz
 * Licensed under the [GPL](http://www.gnu.org/licenses/gpl.html).
 */

package cz.sysrq.treosms;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.io.FileNotFoundException;

/**
 * Application main entry point.
 *
 * @author wictor
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Options options = createApplicationOptions();
        CommandLine commandLine = null;
        CommandLineParser parser = new DefaultParser();
        try {
            commandLine = parser.parse(options, args);
        } catch (ParseException e) {
            System.err.println("Argument error: " + e.getMessage());
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("TreoSMS", usageHdr, options, usageFtr, true);
            System.exit(1);
        }

        if (commandLine != null) {
            String inputFileName = commandLine.getOptionValue("i");
            String outputFileName = commandLine.getOptionValue("o");
            int diff = Integer.parseInt(commandLine.getOptionValue("z", "0")); // hours

            try {
                TreoSms treoSms = new TreoSms(inputFileName, outputFileName);
                if (commandLine.hasOption("t")) {
                    treoSms.processToCsv(diff);
                } else if (commandLine.hasOption("r")) {
                    treoSms.processToRecords();
                } else {
                    // list is default anyway
                    treoSms.processToList(diff);
                }
            } catch (FileNotFoundException e) {
                System.err.println("File not found: " + e.getMessage());
                System.exit(1);
            }
        } else {
            throw new IllegalStateException("Arguments object empty although no exception thrown");
        }

    }

    static final String usageHdr = "Reads Palm Treo 680 file Messages_Database.PDB and converts to selected output.";
    static final String usageFtr = "\nEXAMPLE:\n" +
            " 1. To convert your messages to CSV (this is what you probably want) using zone GMT+1:\n" +
            "   java -jar \"TreoSMS.jar\" -z +1 -t -i Messages_Database.PDB -o sms.csv\n\n" +
            "https://gitlab.com/wictor/TreoSMS\nwictor@sysrq.cz";

    static Options createApplicationOptions() {
        Options options = new Options();
        OptionGroup outputOptGrp = new OptionGroup();
        outputOptGrp.addOption(new Option("t", "csv", false, "CSV output format: convert the messages to the CSV format"));
        outputOptGrp.addOption(new Option("l", "list", false, "list output format: show summary information about input content (default)"));
        outputOptGrp.addOption(new Option("r", "records", false, "debug output format: dumps all records from input"));
        options.addOptionGroup(outputOptGrp);
        Option inputOpt = new Option("i", "input", true, "specify input PDB file");
        inputOpt.setRequired(true);
        options.addOption(inputOpt);
        options.addOption("o", "output", true, "specify output file instead of stdout (default is stdout - console)");
        options.addOption("z", "timediff", true, "specify timezone difference in hours (default = 0, GMT+0)");
        return options;
    }

}
