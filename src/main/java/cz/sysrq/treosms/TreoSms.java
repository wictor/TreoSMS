/*
 * Project: TreoSMS 2008-01-22 - https://gitlab.com/wictor/TreoSMS
 * Author: wictor@sysrq.cz
 * Licensed under the [GPL](http://www.gnu.org/licenses/gpl.html).
 */

package cz.sysrq.treosms;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import cz.sysrq.treosms.data.Message;
import org.jSyncManager.API.Protocol.Util.DLPDatabase;
import org.jSyncManager.API.Protocol.Util.DLPRecord;
import org.jSyncManager.API.Protocol.Util.DatabaseFormatException;
import cz.sysrq.treosms.data.ParsingResult;
import cz.sysrq.treosms.format.CsvFormatter;
import cz.sysrq.treosms.format.MessageFormatter;
import cz.sysrq.treosms.format.SummaryFormatter;
import cz.sysrq.treosms.parser.MessageParser;
import cz.sysrq.treosms.parser.Treo680MsgParser;

/**
 * Palm Treo SMS processor
 *
 * @author wictor
 */
public final class TreoSms {

    private final PrintStream print;
    private final File inputFile;
    private final File outputFile;

    public TreoSms(String inputFileName, String outputFileName) throws FileNotFoundException {
        this.inputFile = checkFile(inputFileName);
        // TODO VSV: check if both file objects points to the same file
        if (outputFileName != null) {
            this.outputFile = checkFile(outputFileName);
            print = new PrintStream(new FileOutputStream(this.outputFile));
        } else {
            this.outputFile = null;
            print = System.out;
        }

    }

    private static File checkFile(String fileName) throws FileNotFoundException {
        File file = new File(fileName);
        if(!file.exists()) {
            throw new FileNotFoundException("File \"" + fileName + "\" not found");
        }
        return file;
    }

    /**
     * Process the message database and print the result using CSV formatter.
     *
     * @param timeZoneDiff difference in hours between GMT and current timezone
     */
    public void processToCsv(int timeZoneDiff) {
        process(new CsvFormatter(timeZoneDiff * 3600));
    }

    /**
     * Process the message database and print the result using summary formatter.
     *
     * @param timeZoneDiff difference in hours between GMT and current timezone
     */
    public void processToList(int timeZoneDiff) {
        process(new SummaryFormatter(timeZoneDiff * 3600));
    }

    private void process(MessageFormatter formatter) {
        MessageParser messageParser = new Treo680MsgParser();
        try {
            DLPDatabase db = DLPDatabase.importFromFile(inputFile);
            ParsingResult result = messageParser.parseDb(db);
            for (Message msg : result.getMessages()) {
                String s = formatter.format(msg);
                print.println(s);
            }
        } catch (DatabaseFormatException ex) {
            System.err.println("Database format error:" + ex);
        }
    }

    public void processToRecords() {
        MessageParser messageParser = new Treo680MsgParser();
        try {
            print.println("Parsing file " + inputFile + " in DEBUG mode...");
            DLPDatabase db = DLPDatabase.importFromFile(inputFile);
            ParsingResult result = messageParser.parseDb(db);

            print.println("Records          : " + db.getElements());
            print.println("SMS found        : " + result.getMessages().size());
            print.println("Trsm records     : " + countTrsm(db));
            print.println("Rejected records : " + result.getSkippedRecords().size());
            print.println("Unrecognized     : " + result.getFailedRecords().size());
        } catch (DatabaseFormatException ex) {
            System.err.println("Database format error:" + ex);
        }
    }

    private static int countTrsm(DLPDatabase db) {
        int count = 0;
        for (int i = 0; i < db.getElements(); i++) { //for all rec
            DLPRecord rec = (DLPRecord) db.getElement(i);
            if (containsTrsm(rec.getData())) {
                count++;
            }
        }
        return count;
    }

    /**
     * Checks if the buffer contain a "Trsm" char sequence. It is useful for
     * deciding, if the buffer contain a SMS.
     * @param buf is a byte buffer (single record)
     * @return true if "Trsm" found
     */
    private static boolean containsTrsm(byte[] buf) {
        for (int pos = 0; pos < buf.length - 3; pos++) {
            if (buf[pos] == 0x54 && buf[pos + 1] == 0x72 && buf[pos + 2] == 0x73 && buf[pos + 3] == 0x6d) {
                return true;
            }
        }
        return false;
    }
}
