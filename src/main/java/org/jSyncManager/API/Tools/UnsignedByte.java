// --------------------------------------------------------------------------
// The jSyncManager Project -- Source File.
// Copyright (c) 1998 - 2004 Brad BARCLAY <bbarclay@jsyncmanager.org>
// --------------------------------------------------------------------------
// OSI Certified Open Source Software
// --------------------------------------------------------------------------
//
// This library is free software; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as published 
// by the Free Software Foundation; either version 2.1 of the License, or 
// (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of 
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public 
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// --------------------------------------------------------------------------
// $Id: UnsignedByte.java,v 3.11 2005/02/11 20:11:17 yaztromo Exp $
// --------------------------------------------------------------------------

package org.jSyncManager.API.Tools;

// ==========================================================================

/** A simple class for manipulating standard Java signed bytes as if 
  * they were Unsigned bytes.
  * This class is populated with static methods, and thus 
  * cannot be instantiated.
  * @author Brad BARCLAY &lt;bbarclay@jsyncmanager.org&gt;
  * <br>Last modified by: $Author: yaztromo $ on $Date: 2005/02/11 20:11:17 $.
  * @version $Revision: 3.11 $
  */

public final class UnsignedByte {

// ==========================================================================

/** The class cannot be constructed.
  */

private UnsignedByte() {
   // This class connot be constructed.
} // end-constructor

// ==========================================================================

/** Increments the given byte by one.
  * Note that if b = 255 unsigned (-1 signed), incrementing it by one
  * will return zero.
  * @param b the byte to increment.
  * @return b incremented by 1.
  */

public static byte increment(byte b) {
	if(b == 127) return -128;
	else return ++b;
} // end-static-method

// --------------------------------------------------------------------------

/** Returns the value of the specified byte in unsigned form as an integer.
  * @param b the byte the unsign and return as an int.
  * @return unsigned b represened as an integer.
  */

public static int intValue(byte b) {
	if(b >= 0) return b;
	else return 256 + b;
} // end-static-method

// --------------------------------------------------------------------------

/** Returns the string representation of the specified byte
  * in unsigned form as a two-digit hex value.
  * @param b the byte to represent as a hex string
  * @return b represented as a hex string.
  */

public static String toString(byte b) {
	int i = intValue(b);
	char c;
	if(i / 16 < 10) c = (char)(i / 16 + 48);
	else c = (char)(i / 16 + 55);

	char c1;
	if(i % 16 < 10) c1 = (char)(i % 16 + 48);
	else c1 = (char)(i % 16 + 55);

	return new String("" + c + "" + c1);
} // end-static-method

// --------------------------------------------------------------------------

/** Converts two bytes into a single char value.
  * @param b1 the first (high order) byte.
  * @param b2 the second (low-order) byte.
  * @return b1 and b2 as a single char.
  */

public static char unsignedBytes2Char(byte b1, byte b2) {
   return (char)((intValue(b1) << 8) | intValue(b2));
} // end-static-method

// --------------------------------------------------------------------------

/** Converts four bytes into a single int value.
  * @param b1 the first (high order) byte.
  * @param b2 the second byte.
  * @param b3 the third byte.
  * @param b4 the fourth (low-order) byte.
  * @return b1, b2, b3 and b4 as a single int.
  */

public static int unsignedBytes2Int(byte b1, byte b2, byte b3, byte b4) {
   return (intValue(b1) << 24) | (intValue(b2) << 16) | (intValue(b3) << 8) | intValue(b4);
} // end-static-method

// --------------------------------------------------------------------------

/** Returns an array of bytes as a human-readable String.
  * This method will return a String, containing 16 hex digits per row, followed by 
  * their ASCII representation if an alpha-numeric or punctuation character, or a period ('.')
  * if a non-printable character.
  * @param b an array of bytes.
  * @param length the number of bytes to dump.
  * @return a String containing the array of bytes as a human-readable String.
  */

public static String toString(byte[] b, int length) {
   return toString(b, 0, length);
} // end-static-method

// --------------------------------------------------------------------------

/** Returns an array of bytes as a human-readable String.
  * This method will return a String, containing 16 hex digits per row, followed by 
  * their ASCII representation if an alpha-numeric or punctuation character, or a period ('.')
  * if a non-printable character.
  * @param b an array of bytes.
  * @param startPos the index in the array to start from.
  * @param length the number of bytes to dump.
  * @return a String containing the array of bytes as a human-readable String.
  */

public static String toString(byte[] b, int startPos, int length) {
   int i=startPos, j;
   int startOfLine=0;
   int endPos = startPos+length;
   StringBuffer sb=new StringBuffer();

   while (i<endPos && i<b.length) {
      sb.append(toString(b[i]));
      sb.append(' ');

      // If this byte mod 8 == 0, we add a seperator character
      if ((i+1)%8==0 && i!=0) sb.append("| ");

      // If this byte mod 16 == 0, we've reached the EOL.  Print out the last 16 bytes
      if ((i+1)%16==0 && i!=0) {
         for (j=startOfLine;j<startOfLine+16;j++) {
            if (b[j] >= 0x20 && b[j] <0x7F) {
               sb.append((char)b[j]);
            } else {
               sb.append('.');
            } /* endif */
         } /* endfor */
         startOfLine+=16;
         sb.append('\n');
      } /* endif */
      i++;           // Increment to next character
   } /* endwhile */

   // Chances are good that when we get here, the last line will have contained less than
   // 16 bytes.  Thus, we should dump the ASCII for them again here.
   int bytesInLine = b.length%16;
   int charsInLine = bytesInLine*3;    // "XX "
   if (bytesInLine>=8) charsInLine+=2;  // Add 2 if we've also printed the "| " for this line.   

   if (charsInLine!=0) {
      // First, space us out to the 50th character position
      for (j=0;j<50-charsInLine;j++) {
         sb.append(' ');
      } /* endfor */

      sb.append("| ");

      for (j=startOfLine;j<length;j++) {
         if (b[j] >= 0x20 && b[j] <0x7F) {
            sb.append((char)b[j]);
         } else {
            sb.append('.');
         } /* endif */
      } /* endfor */
      sb.append('\n');
   } /* endif */

   return sb.toString();   
} // end-static-method

// --------------------------------------------------------------------------

/** Takes an array of unsigned bytes and converts it to an array of char.
  * Note that this method does <B>not</B> do any character conversion.
  * It merely takes two 8-bit bytes and concatenates them into a single 16-bit word.
  * If an odd number of bytes are input, the last byte will be ignored.
  * @param inBuffer the input bufffer of bytes.
  * @return the array of bytes converted to an array of char.
  */
public static char[] unsignedByteArrayToCharArray(byte[] inBuffer) {
   char[] ret = new char[inBuffer.length/2];
   int k=0;
   
   for(int i=0;i<ret.length;i++) ret[i]=UnsignedByte.unsignedBytes2Char(inBuffer[k++], inBuffer[k++]);
   return ret;
} // end-static-method

// --------------------------------------------------------------------------

/** Returns an array of bytes as a human-readable String.
  * This method will return a String, containing 16 hex digits per row, followed by 
  * their ASCII representation if an alpha-numeric or punctuation character, or a period ('.')
  * if a non-printable character.
  * @param b an array of bytes.
  * @return a String containing the array of bytes as a human-readable String.
  */

public static String toString(byte[] b) {
   if (b!=null) return UnsignedByte.toString(b, b.length);
   else return "null data array";
} // end-static-method

// --------------------------------------------------------------------------

/** Copies a char to the specified byte array, placing its byte values at the specified index.
  * @param value the char to copy into the byte array.
  * @param dest the byte array to copy into.
  * @param index the index in the byte array to copy into.
  * @return the index of the next byte after the last byte in the copied char.
  */
public static int copyCharToByteArray(char value, byte[] dest, int index) {
   dest[index] = (byte)((value & 0xFF00) >> 8);
   dest[index+1] = (byte)(value & 0x00FF);
   return index+2;
} // end-static-method

// --------------------------------------------------------------------------

/** Copies an int to the specified byte array, placing its byte values at the specified index.
  * @param value the int to copy into the byte array.
  * @param dest the byte array to copy into.
  * @param index the index in the byte array to copy into.
  * @return the index of the next byte after the last byte in the copied int.
  */
public static int copyIntToByteArray(int value, byte[] dest, int index) {
   dest[index]   = (byte)((value & 0xFF000000) >> 24);
   dest[index+1] = (byte)((value & 0x00FF0000) >> 16);
   dest[index+2] = (byte)((value & 0x0000FF00) >> 8);
   dest[index+3] = (byte)((value & 0x000000FF));
   
   return index+4;
} // end-static-method

// --------------------------------------------------------------------------

/** Copies a long to the specified byte array, placing its last 4 byte values at the specified index.
  * This method is used in cases where we store data as a long, but need to transmit only its last four bytes.
  * This is typically done in cases where we need to work with unsigned ints, like in DLP_Date.
  * @param value the long to copy into the byte array.
  * @param dest the byte array to copy into.
  * @param index the index in the byte array to copy into.
  * @return the index of the next byte after the last byte in the copied long.
  */
public static int copyLongToByteArray4(long value, byte[] dest, int index) {
   dest[index]   = (byte)((value & 0x00000000FF000000L) >> 24);
   dest[index+1] = (byte)((value & 0x0000000000FF0000L) >> 16);
   dest[index+2] = (byte)((value & 0x000000000000FF00L) >> 8);
   dest[index+3] = (byte)((value & 0x00000000000000FFL));
   
   return index+4;
} // end-static-method

// --------------------------------------------------------------------------

/** Copies a long to the specified byte array at the specified index.
  * Unlike with copyLongToByteArray4, this method will copy all 8 bytes of the long into the array.
  * This is currently not used by the jSyncManager, but is provided here for completeness.
  * @param value the long to copy into the byte array.
  * @param dest the byte array to copy into.
  * @param index the index in the byte array to copy into.
  * @return the index of the next byte after the last byte in the copied long.
  */
public static int copyLongToByteArray(long value, byte[] dest, int index) {
   dest[index]   = (byte)((value & 0xFF00000000000000L) >> 56);
   dest[index+1] = (byte)((value & 0x00FF000000000000L) >> 48);
   dest[index+2] = (byte)((value & 0x0000FF0000000000L) >> 40);
   dest[index+3] = (byte)((value & 0x000000FF00000000L) >> 32);
   dest[index+4] = (byte)((value & 0x00000000FF000000L) >> 24);
   dest[index+5] = (byte)((value & 0x0000000000FF0000L) >> 16);
   dest[index+6] = (byte)((value & 0x000000000000FF00L) >> 8);
   dest[index+7] = (byte)((value & 0x00000000000000FFL));
   
   return index+8;
} // end-static-method

// --------------------------------------------------------------------------

/** Converts four bytes into a single unsigned int (represented as a long) value.
  * @param b1 the first (high order) byte.
  * @param b2 the second byte.
  * @param b3 the third byte.
  * @param b4 the fourth (low-order) byte.
  * @return b1, b2, b3 and b4 as a single long.
  */
public static long unsignedBytes2Long4(byte b1, byte b2, byte b3, byte b4) {
   return (((long)intValue(b1) << 24)
         | ((long)intValue(b2) << 16)
         | ((long)intValue(b3) << 8)
         | (long)intValue(b4));
} // end-static-method

// --------------------------------------------------------------------------

/** Converts eight bytes into a single long value.
  * @param b1 the first (high order) byte.
  * @param b2 the second byte.
  * @param b3 the third byte.
  * @param b4 the fourth byte.
  * @param b5 the fifth byte.
  * @param b6 the sixth byte.
  * @param b7 the seventh byte.
  * @param b8 the eigth (low-order) byte.
  * @return b1, b2, b3, b4, b5, b6, b7, and b8 as a single long.
  */
public static long unsignedBytes2Long(byte b1, byte b2, byte b3, byte b4, byte b5, byte b6, byte b7, byte b8) {
   return (((long)intValue(b1) << 56)
         | ((long)intValue(b2) << 48)
         | ((long)intValue(b3) << 40)
         | ((long)intValue(b4) << 32)
         | ((long)intValue(b5) << 24)
         | ((long)intValue(b6) << 16)
         | ((long)intValue(b7) << 8)
         | (long)intValue(b8));
} // end-static-method

// ==========================================================================

} // end-class
