// --------------------------------------------------------------------------
// The jSyncManager Project -- Source File.
// Copyright (c) 1998 - 2004 Brad BARCLAY <bbarclay@jsyncmanager.org>
// --------------------------------------------------------------------------
// OSI Certified Open Source Software
// --------------------------------------------------------------------------
//
// This library is free software; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as published 
// by the Free Software Foundation; either version 2.1 of the License, or 
// (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of 
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public 
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// --------------------------------------------------------------------------
// $Id: DLPFunctionCallException.java,v 3.2 2004/07/14 04:31:14 yaztromo Exp $
// --------------------------------------------------------------------------

package org.jSyncManager.API.Protocol;

// ==========================================================================

/** The DLPFunctionCallException class.
  * This exception is thrown by methods in the JHotSync class
  * whenever a DLP error is reported by the Palm.
  * @author Brad BARCLAY &lt;bbarclay@jsyncmanager.org&gt;
  * <br>Last modified by: $Author: yaztromo $ on $Date: 2004/07/14 04:31:14 $.
  * @version $Revision: 3.2 $
  */

public class DLPFunctionCallException extends Exception {

	private static final long serialVersionUID = 7273168373181220226L;

	/** This error value is returned when no DLP Error code is available.
	  * This can occur when the JHotSync class runs into an internal error in
	  * one of its methods.  See the error text for details.
	  */
	public static final int NO_DLP_ERROR = Integer.MAX_VALUE;

   /** The error code associated with this function call exception.  */
	private int errorCode;

// ==========================================================================

/** Constructs a new DLPFunctionCallException object with the given DLP
  * error code.
  * See the DLP_Packet class for a list of error codes.
  * @param errorCode the DLP error code this exception represents.
  * @see DLP_Packet
  */

public DLPFunctionCallException(int errorCode) {
	super();
	this.errorCode=errorCode;
} // end-constructor

// --------------------------------------------------------------------------

/** Constructs a new DLPFunctionCallException object with the given DLP
  * error code and text message.
  * @param s a String containing a description of the exception caught.
  * See the DLP_Packet class for a list of error codes.
  * @param errorCode the DLP error code this exception represents.
  * @see DLP_Packet
  */

public DLPFunctionCallException(String s, int errorCode) {
	super(s);
	this.errorCode=errorCode;
} // end-constructor

// ==========================================================================

/**
 * Returns the error code associated with this DLPFunctionCallException.
 * @return the error code associated with this DLPFunctionCallException.
 * @see DLP_Packet
 * @see DLPFunctionCallException#NO_DLP_ERROR
 */

public int getErrorCode() {
	return errorCode;
} // end-method

// ==========================================================================

} // end-class

