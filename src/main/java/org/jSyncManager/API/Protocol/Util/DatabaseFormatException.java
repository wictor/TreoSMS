// --------------------------------------------------------------------------
// The jSyncManager Project -- Source File.
// Copyright (c) 1998 - 2004 Brad BARCLAY <bbarclay@jsyncmanager.org>
// --------------------------------------------------------------------------
// OSI Certified Open Source Software
// --------------------------------------------------------------------------
//
// This library is free software; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as published 
// by the Free Software Foundation; either version 2.1 of the License, or 
// (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of 
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public 
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// --------------------------------------------------------------------------
// $Id: DatabaseFormatException.java,v 3.2 2004/07/14 04:31:14 yaztromo Exp $
// --------------------------------------------------------------------------

package org.jSyncManager.API.Protocol.Util;

// ==========================================================================

/** The DatabaseFormatException class.
  * This exeception is generated when there is a problem with 
  * the format of a database object.
  * @author Brad BARCLAY &lt;bbarclay@jsyncmanager.org&gt;
  * <br>Last modified by: $Author: yaztromo $ on $Date: 2004/07/14 04:31:14 $.
  * @version $Revision: 3.2 $
  */
  
public class DatabaseFormatException extends Exception {

// ==========================================================================

	private static final long serialVersionUID = -4429159712373454415L;

/** Constructs a new DatabaseFormatException object.
  */

public DatabaseFormatException() {
   super();
} // end-constructor

// --------------------------------------------------------------------------

/** Constructs a new DatabaseFormatException object with the given message.
  * @param s a String containing the exception message text.
  */

public DatabaseFormatException(String s) {
	super(s);
} // end-constructor

// ==========================================================================

} // end-class
