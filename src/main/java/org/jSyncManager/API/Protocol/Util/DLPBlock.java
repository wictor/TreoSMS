// --------------------------------------------------------------------------
// The jSyncManager Project -- Source File.
// Copyright (c) 1998 - 2004 Brad BARCLAY <bbarclay@jsyncmanager.org>
// --------------------------------------------------------------------------
// OSI Certified Open Source Software
// --------------------------------------------------------------------------
//
// This library is free software; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as published 
// by the Free Software Foundation; either version 2.1 of the License, or 
// (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of 
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public 
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// --------------------------------------------------------------------------
// $Id: DLPBlock.java,v 3.4 2005/02/11 20:11:09 yaztromo Exp $
// --------------------------------------------------------------------------

package org.jSyncManager.API.Protocol.Util;

import org.jSyncManager.API.Protocol.DLPFunctionCallException;
import org.jSyncManager.API.Tools.UnsignedByte;

import java.io.Serializable;

// ==========================================================================

/** A class that represents database information blocks.
  * This class stores a data block.  This is typically used in 
  * Palm databases to store the application block and the sort block.
  * @author Brad BARCLAY &lt;bbarclay@jsyncmanager.org&gt;
  * <br>Last modified by: $Author: yaztromo $ on $Date: 2005/02/11 20:11:09 $.
  * @version $Revision: 3.4 $
  */

public class DLPBlock implements Serializable {
   /** Holds the size of the block.                   */
	protected int blockSize = 0;

   /** Holds the data array representing this block.  */
	protected byte data[] = null;

   private static final long serialVersionUID = 5859895257389991635L;

// ==========================================================================

/** Creates an empty block, with size 0.
  */
public DLPBlock() {
} // end-constructor

// --------------------------------------------------------------------------

/** Creates a new block based on the input byte array.
  * This construct is typically used when the block is read from the handheld via the DLP protocol,
  * as it includes two extra bytes of sizing data that don't exist in PDB files.
  * @param inputData the input byte array to be parsed.
  * @exception DLPFunctionCallException thrown if any parsing problem is encountered.
  */
public DLPBlock(byte inputData[]) throws DLPFunctionCallException {
	data = new byte[inputData.length - 2];
	int i = 0;

	try {
		setBlockSize(UnsignedByte.unsignedBytes2Char(inputData[i++], inputData[i++]));
		System.arraycopy(inputData, i, data, 0, data.length);
	} catch(Throwable throwable) {
		throw new DLPFunctionCallException(throwable.toString(), DLPFunctionCallException.NO_DLP_ERROR);
	}
} // end-constructor

// --------------------------------------------------------------------------

/** Creates a new block based on the input byte array.
  * This constructor is best suited for uses where the input data does not contain the two-byte
  * blocksize preceeding the data in the input byte array.
  * @param inputData the input byte array to be parsed.
  * @param blockSize the size of the input block.
  * @exception DLPFunctionCallException thrown if any parsing problem is encountered.
  */
public DLPBlock(byte inputData[], char blockSize) throws DLPFunctionCallException {
	data = new byte[inputData.length];

	try {
		setBlockSize(blockSize);
		System.arraycopy(inputData, 0, data, 0, data.length);
	} catch(Throwable throwable) {
		throw new DLPFunctionCallException(throwable.toString(), DLPFunctionCallException.NO_DLP_ERROR);
	}
} // end-constructor

// ==========================================================================

/** Generates the data block from a set of class fields.
  * This method is called to take the input fields in the block class, and
  * to form a byte array representation of the block.
  * This method is intended for implementation by subclasses.  In this class,
  * its implementation is null.
  */
protected void generateData() {
} // end-method

// --------------------------------------------------------------------------

/** Returns the size of this block.
  * This is the size of the block as reported by the Palm at generation time.  To get the
  * actual number of bytes in the current data array, you should instead use getData().length.
  * @return the size of this block.
  */
public int getBlockSize() {
	return blockSize;
} // end-method

// --------------------------------------------------------------------------

/** Retreives the byte array containing the data in this block.
  * @return the byte array containing the data in this block.
  */
public byte[] getData() {
   generateData();
	return data;
} // end-method

// --------------------------------------------------------------------------

/** Parses the data block into a set of fields.
  * This method is called to take the input byte array in the block class, and
  * to parse it into its representitive fields.
  * This method is intended for implementation by subclasses.  In this class,
  * its implementation is null.
  * @throws ParseException if an error is encountered while parsing the data fields.
  */
protected void parseFields() throws ParseException {
} // end-method

// --------------------------------------------------------------------------

/** Sets the size of this block.
  * @param i the size of this block.
  */
private void setBlockSize(int i) {
	blockSize = i;
} // end-method

// --------------------------------------------------------------------------

/** Sets the data byte array for this block.
  * @param data the byte array containing block information.
  */
public void setData(byte data[]) throws ParseException {
	this.data = data;
   parseFields();
} // end-method

// --------------------------------------------------------------------------

/** Returns a human-readable hex representation of the block data.
  * @return java.lang.String
  */
public String toString() {
   StringBuffer sb=new StringBuffer();
   generateData();

   sb.append("Block Size:     ");sb.append(blockSize);sb.append("\n\n");
   sb.append(UnsignedByte.toString(data));

   return sb.toString();
} // end-method

// ==========================================================================

} // end-class

