// --------------------------------------------------------------------------
// The jSyncManager Project -- Source File.
// Copyright (c) 1998 - 2004 Brad BARCLAY <bbarclay@jsyncmanager.org>
// --------------------------------------------------------------------------
// OSI Certified Open Source Software
// --------------------------------------------------------------------------
//
// This library is free software; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as published 
// by the Free Software Foundation; either version 2.1 of the License, or 
// (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of 
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public 
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// --------------------------------------------------------------------------
// $Id: DLPDatabaseInfo.java,v 3.11 2005/07/27 07:46:55 yaztromo Exp $
// --------------------------------------------------------------------------

package org.jSyncManager.API.Protocol.Util;

import org.jSyncManager.API.Protocol.DLPFunctionCallException;
import org.jSyncManager.API.Tools.StringManipulator;
import org.jSyncManager.API.Tools.UnsignedByte;

import java.io.Serializable;


// ==========================================================================

/** A class to store information on a handheld database.
  * This class holds the information on a single database.
  * @author Brad BARCLAY &lt;bbarclay@jsyncmanager.org&gt;
  * <br>Last modified by: $Author: yaztromo $ on $Date: 2005/07/27 07:46:55 $.
  * @version $Revision: 3.11 $
  */
public class DLPDatabaseInfo implements Serializable {
   /** A flag to denote that this database should be excluded from synchronization.    */
   public static final byte EXCLUDE_FROM_SYNC = -128;

   /** A flag to denote that this database is RAM based.                               */
   public static final byte RAM_BASED = (byte)0x40;

   /** A value denoting an unknown database index.                                     */
   public static final char UNKNOWN_DB_INDEX= '\uFFFF';

   private byte miscFlags=0;
   private char databaseFlags=0;
   private int type=0;
   private int creator=0;
   private int version=0;
   private int modificationNumber=0;
   private DLP_Date creationTime=null;
   private DLP_Date backupTime=null;
   private DLP_Date modificationTime=null;
   private String name=null;
   private char databaseIndex=0;

   private static final long serialVersionUID = 9154021653478283089L;

// ==========================================================================

/** DLPDatabaseInfo default constructor.
 */
public DLPDatabaseInfo() {
   super();
} // end-constructor

// --------------------------------------------------------------------------

/** DLPDatabaseInfo constructor, given the byte array data block.
 * @param dbInfoData byte[] data block.
 * @throws DLPFunctionCallException if a parsing error occurs.
 */
public DLPDatabaseInfo(byte dbInfoData[]) throws DLPFunctionCallException {
   int i = 0;

   try {
      int j = UnsignedByte.intValue(dbInfoData[i++]);
      if((dbInfoData.length < j) | (j > dbInfoData.length + 1)) throw new DLPFunctionCallException("DB Info record size mismatch!", DLPFunctionCallException.NO_DLP_ERROR);
      setMiscFlags(dbInfoData[i++]);
      setDatabaseFlags(UnsignedByte.unsignedBytes2Char(dbInfoData[i++], dbInfoData[i++]));
      setType(UnsignedByte.unsignedBytes2Int(dbInfoData[i++], dbInfoData[i++], dbInfoData[i++], dbInfoData[i++]));
      setCreator(UnsignedByte.unsignedBytes2Int(dbInfoData[i++], dbInfoData[i++], dbInfoData[i++], dbInfoData[i++]));
      setVersion(UnsignedByte.unsignedBytes2Char(dbInfoData[i++], dbInfoData[i++]));
      setModificationNumber(UnsignedByte.unsignedBytes2Int(dbInfoData[i++], dbInfoData[i++], dbInfoData[i++], dbInfoData[i++]));
      byte nameArray[] = new byte[8];
      System.arraycopy(dbInfoData, i, nameArray, 0, 8);
      i+=8;
      
      setCreationTime(new DLP_Date(nameArray));
      System.arraycopy(dbInfoData, i, nameArray, 0, 8);
      i+=8;
      
      setModificationTime(new DLP_Date(nameArray));
      System.arraycopy(dbInfoData, i, nameArray, 0, 8);
      i+=8;

      setBackupTime(new DLP_Date(nameArray));
      setDatabaseIndex(UnsignedByte.unsignedBytes2Char(dbInfoData[i++], dbInfoData[i++]));

      int k=0;
      for (int n=i;dbInfoData[n++]!=0;k++);

      nameArray = new byte[k];
      System.arraycopy(dbInfoData, i, nameArray, 0, nameArray.length);
      i+=nameArray.length;

      setName(new String(nameArray));
   } catch(Throwable throwable) {
      throw new DLPFunctionCallException(throwable.toString(), DLPFunctionCallException.NO_DLP_ERROR);
   }
} // end-constructor

// ==========================================================================

/** Check the database flags against a char indicator to see if the flag is set.
 * @param c char flag to check if set.
 * @return boolean.
 */
public boolean checkDatabaseFlag(char c) {
   return (databaseFlags & c) != 0;
} // end-method

// --------------------------------------------------------------------------

/** Check the Misc flags against a byte indicator to see if the flag is set.
 * @param byte0 byte flag to check if set.
 * @return boolean.
 */
public boolean checkMiscFlag(byte byte0) {
   return (miscFlags & byte0) != 0;
} // end-method

// --------------------------------------------------------------------------

/** Return the last backup time in DLP_Date format.
 * @return DLP_Date.
 */
public DLP_Date getBackupTime() {
   return backupTime;
} // end-method

// --------------------------------------------------------------------------

/** Return the creation time of this database in DLP_Date format.
 * @return DLP_Date.
 */
public DLP_Date getCreationTime() {
   return creationTime;
} // end-method

// --------------------------------------------------------------------------

/** Return the creator ID for this database as an int value.
 * @return int.
 */
public int getCreator() {
   return creator;
} // end-method

// --------------------------------------------------------------------------

/** Return the creator ID for this database as an String value.
 * @return java.lang.String.
 */
public String getCreatorID() {
   byte data[] = new byte[4];
   UnsignedByte.copyIntToByteArray(creator, data, 0);
   return new String(data);
} // end-method

// --------------------------------------------------------------------------

/** Return the database flags as a char value.
 *  @return char databaseFlags.
 */
public char getDatabaseFlags() {
   return databaseFlags;
} // end-method

// --------------------------------------------------------------------------

/** Return the database index as a char value.
 * @return char databaseIndex.
 */
public char getDatabaseIndex() {
   return databaseIndex;
} // end-method

// --------------------------------------------------------------------------

/** Return the Misc flags as a byte value.
 * @return byte miscFlags.
 */
public byte getMiscFlags() {
   return miscFlags;
} // end-method

// --------------------------------------------------------------------------

/** Return the modification number of this database as an int value.
 * @return int modificationNumber.
 */
public int getModificationNumber() {
   return modificationNumber;
} // end-method

// --------------------------------------------------------------------------

/** Return the last modification time of this database as a DLP_Date object.
 * @return DLP_Date modificationTime.
 */
public DLP_Date getModificationTime() {
   return modificationTime;
} // end-method

// --------------------------------------------------------------------------

/** Return the name of this database as a String value.
 * @return java.lang.String name.
 */
public String getName() {
   return name;
} // end-method

// --------------------------------------------------------------------------

/** Return the type of this database as an int value.
 * @return int type.
 */
public int getType() {
   return type;
} // end-method

// --------------------------------------------------------------------------

/** Return the type ID of this database as a String value.
 * @return java.lang.String type ID.
 */
public String getTypeID() {
   byte data[] = new byte[4];
   UnsignedByte.copyIntToByteArray(type, data, 0);
   return new String(data);
} // end-method

// --------------------------------------------------------------------------

/** Return the unique DB ID of this database as a String value.
 * @return java.lang.String.
 */
String getUniqueDBID() {
   return StringManipulator.generateValidFilename(getName()).replace(' ', '_');
} // end-method

// --------------------------------------------------------------------------

/** Return the version of this database as an int value.
 * @return int version.
 */
public int getVersion() {
   return version;
} // end-method

// --------------------------------------------------------------------------

/** Set the backup time from a DLP_Date object.
 * @param dlp_date DLP_Date object.
 */
public void setBackupTime(DLP_Date dlp_date) {
   backupTime = dlp_date;
} // end-method

// --------------------------------------------------------------------------

/** Set the creation time from a DLP_Date object.
 * @param dlp_date DLP_Date object.
 */
public void setCreationTime(DLP_Date dlp_date) {
   creationTime = dlp_date;
} // end-method

// --------------------------------------------------------------------------

/** Set the creator ID from an int value.
 * @param i int value of new creator ID.
 */
public void setCreator(int i) {
   creator = i;
} // end-method

// --------------------------------------------------------------------------

/** Set the creator of this database from a four-chcracter String value.
  * If the String exceeds 4 characters, only the first four will be used.
  * @param id the named creator identifier.
  */
public void setCreatorID(String id) {
   byte[] idBytes = id.getBytes();
   creator = UnsignedByte.unsignedBytes2Int(idBytes[0], idBytes[1], idBytes[2], idBytes[3]);
} // end-method

// --------------------------------------------------------------------------

/** Set the database flags from a char value.
 * @param c char value of database flags.
 */
public void setDatabaseFlags(char c) {
   databaseFlags = c;
} // end-method

// --------------------------------------------------------------------------

/** Set the database index from a char value.
 * @param c char value of new database index.
 */
public void setDatabaseIndex(char c) {
   databaseIndex = c;
} // end-method

// --------------------------------------------------------------------------

/** Set the misc flags from a byte value.
 * @param flags byte value of misc flags.
 */
public void setMiscFlags(byte flags) {
   miscFlags = flags;
} // end-method

// --------------------------------------------------------------------------

/** Set the modification number from an int value.
 * @param i int value of new modification number.
 */
public void setModificationNumber(int i) {
   modificationNumber = i;
} // end-method

// --------------------------------------------------------------------------

/** Set the modification number from an DLP_Date value.
 * @param dlp_date new DLP_Date modification time value.
 */
public void setModificationTime(DLP_Date dlp_date) {
   modificationTime = dlp_date;
} // end-method

// --------------------------------------------------------------------------

/** Set the name of this database.
 * @param s String new name.
 */
public void setName(String s) {
   name = s;
} // end-method

// --------------------------------------------------------------------------

/** Set the type of this database from an int value.
 * @param i int new type value.
 */
public void setType(int i) {
   type = i;
} // end-method

// --------------------------------------------------------------------------

/** Set the type of this database from a four-chcracter String value.
  * If the String exceeds 4 characters, only the first four will be used.
  * @param id the named type identifier.
  */
public void setTypeID(String id) {
   byte[] idBytes = id.getBytes();
   type = UnsignedByte.unsignedBytes2Int(idBytes[0], idBytes[1], idBytes[2], idBytes[3]);
} // end-method

// --------------------------------------------------------------------------

/** Set the version of this database from an int value.
 * @param i int new version value.
 */
public void setVersion(int i) {
   version = i;
} // end-method

// --------------------------------------------------------------------------

/** Prepares this database info object to be used when writing a database that is already open.
  * If this database is already open on the handheld, set its write flags to install newer,
  * and modify its name by either adding or removing a space from the end.
  */
public void setupToInstallNewer() {
   setDatabaseFlags((char)(getDatabaseFlags() | DLPDatabase.INSTALL_NEWER));
   if (getName().endsWith(" ")) setName(getName().trim());
   else setName(getName()+" ");
} // end-method

// --------------------------------------------------------------------------

/** Tests to see wether or not this DLPDatabaseInfo object has the specified database flag(s) set.
  * @param testFlag the flag(s) to test.
  * @return <B>true</B> if the specified flag(s) are set, <B>false</B> otherwise.
  */
public boolean hasDatabaseFlag(char testFlag) {
   return (databaseFlags & testFlag) == testFlag;
} // end-method

// --------------------------------------------------------------------------

/** Tests to see wether or not this DLPDatabaseInfo object has the specified misc flag(s) set.
  * @param testFlag the flag(s) to test.
  * @return <B>true</B> if the specified flag(s) are set, <B>false</B> otherwise.
  */
public boolean hasMiscFlag(byte testFlag) {
   return (miscFlags & testFlag) == testFlag;
} // end-method

// --------------------------------------------------------------------------


/** Return a String representation of this database info object.
 * @return String.
 */
public String toString() {
   StringBuffer stringbuffer = new StringBuffer();
   stringbuffer.append("  Database Flags:      ");
   if(hasDatabaseFlag(DLPDatabase.RESOURCE_DATABASE)) stringbuffer.append("RESOURCE_DATABASE ");
   if(hasDatabaseFlag(DLPDatabase.READ_ONLY)) stringbuffer.append("READ_ONLY ");
   if(hasDatabaseFlag(DLPDatabase.APP_INFO_DIRTY)) stringbuffer.append("APP_INFO_DIRTY ");
   if(hasDatabaseFlag(DLPDatabase.BACKUP_FLAG)) stringbuffer.append("BACKUP_FLAG ");
   if(hasDatabaseFlag(DLPDatabase.INSTALL_NEWER)) stringbuffer.append("INSTALL_NEWER ");
   if(hasDatabaseFlag(DLPDatabase.RESET_AFTER_INSTALL)) stringbuffer.append("RESET_AFTER_INSTALL ");
   if(hasDatabaseFlag(DLPDatabase.COPY_PREVENTION)) stringbuffer.append("COPY_PREVENTION ");
   if(hasDatabaseFlag(DLPDatabase.DB_OPEN)) stringbuffer.append("DB_OPEN ");
   if(databaseFlags == 0) stringbuffer.append("NONE");
   stringbuffer.append("\n");
   stringbuffer.append("  Misc. Flags:         ");

   if(hasMiscFlag(EXCLUDE_FROM_SYNC)) stringbuffer.append("EXCLUDE_FROM_SYNC ");
   
   if(hasMiscFlag(RAM_BASED)) stringbuffer.append("RAM_BASED ");
   else stringbuffer.append("ROM_BASED ");
   stringbuffer.append("\n");

   stringbuffer.append("  Database Name:       " + name.trim() + "\n");
   stringbuffer.append("  Database Type #:     " + getType() + "\n");
   stringbuffer.append("  Database Creator #:  " + getCreator() + "\n");
   stringbuffer.append("  Database Type:       " + getTypeID() + "\n");
   stringbuffer.append("  Database Creator:    " + getCreatorID() + "\n");
   stringbuffer.append("  Database Version:    " + version + "\n");
   stringbuffer.append("  Modification Number: " + modificationNumber + "\n");
   stringbuffer.append("  Creation Time:       " + creationTime.toString() + "\n");
   stringbuffer.append("  Backup Time:         " + backupTime.toString() + "\n");
   stringbuffer.append("  Modification Time:   " + modificationTime.toString() + "\n");
   stringbuffer.append("  Database Index:      ");

   if(getDatabaseIndex() == UNKNOWN_DB_INDEX) stringbuffer.append("Unknown\n");
   else stringbuffer.append((int)getDatabaseIndex());

   return stringbuffer.toString();
} // end-method

// ==========================================================================

} // end-class

