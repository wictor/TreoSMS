// --------------------------------------------------------------------------
// The jSyncManager Project -- Source File.
// Copyright (c) 1998 - 2004 Brad BARCLAY <bbarclay@jsyncmanager.org>
// --------------------------------------------------------------------------
// OSI Certified Open Source Software
// --------------------------------------------------------------------------
//
// This library is free software; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as published 
// by the Free Software Foundation; either version 2.1 of the License, or 
// (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of 
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public 
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// --------------------------------------------------------------------------
// $Id: ParseException.java,v 3.1 2004/10/29 15:55:02 yaztromo Exp $
// --------------------------------------------------------------------------

package org.jSyncManager.API.Protocol.Util;

// ==========================================================================

/** This class contains the exception thrown when MD5 encryption is not supported
  * on the users platform.
  * @author Brad BARCLAY &lt;bbarclay@jsyncmanager.org&gt;
  * <br>Last modified by: $Author: yaztromo $ on $Date: 2004/10/29 15:55:02 $.
  * @version $Revision: 3.1 $
  */

public class ParseException extends Exception {

	private static final long serialVersionUID = 6580360162224308838L;
	
/** A field to store the location within the array where 
     * the parse exception is believed to have occurred.       */
   private int index = -1;

// ==========================================================================

/** Construct a new ParseException with no details text and no index value.
  */

public ParseException() {
   super();
} // end-constructor

// --------------------------------------------------------------------------

/** Construct a new ParseException with the specified details text String, but no index value.
  * @param s the exception details message to attach to this exception.
  */

public ParseException(String s) {
   super(s);
} // end-constructor

// --------------------------------------------------------------------------

/** Construct a new ParseException with the specified details text String and index value.
  * @param s the exception details message to attach to this exception.
  * @param i the index where the parse excepion is believed to have occurred.
  */

public ParseException(String s, int i) {
   super(s);
   index = i;
} // end-constructor

// ==========================================================================

/** Retrieves the index where the exception occurred.
  * @return the index where the exception occurred.
  */
public int getIndex() {
   return index;
} // end-method

// ==========================================================================

} // end-class
