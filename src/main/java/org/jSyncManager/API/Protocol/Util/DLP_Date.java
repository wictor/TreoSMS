// --------------------------------------------------------------------------
// The jSyncManager Project -- Source File.
// Copyright (c) 1998 - 2004 Brad BARCLAY <bbarclay@jsyncmanager.org>
// --------------------------------------------------------------------------
// OSI Certified Open Source Software
// --------------------------------------------------------------------------
//
// This library is free software; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as published 
// by the Free Software Foundation; either version 2.1 of the License, or 
// (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of 
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public 
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// --------------------------------------------------------------------------
// $Id: DLP_Date.java,v 3.9 2005/05/17 21:13:30 yaztromo Exp $
// --------------------------------------------------------------------------

package org.jSyncManager.API.Protocol.Util;

import org.jSyncManager.API.Tools.UnsignedByte;

import java.io.Serializable;
import java.util.*;


// ==========================================================================

/** A class to hold a Palm DLP protocol Date element.
  * This class holds date information.
  * @author Brad BARCLAY &lt;bbarclay@jsyncmanager.org&gt;
  * <br>Last modified by: $Author: yaztromo $ on $Date: 2005/05/17 21:13:30 $.
  * @version $Revision: 3.9 $
  */
public class DLP_Date implements Serializable {
   /** The year this date represents.                       */
   private char year;

   /** The month this date represents.                      */
   private byte month;

   /** The day this date represents.                        */
   private byte day;

   /** The hour this date represents.                       */
   private byte hour;

   /** The minute this date represents.                     */
   private byte minute;

   /** The second this date represents.                     */
   private byte second;

   private static final long serialVersionUID = -8361916771350447244L;

   public static final long JAN1_1904_MS_FROM_EPOCH = -2082844800000L;

   /** A value for use when calling get/setVFSFileDate in order to specify
     * that you wish to request the files creation date.
     */
   public static final char VFS_CREATION_DATE = (char)1;
   
   /** A value for use when calling get/setVFSFileDate in order to specify
     * that you wish to request the files modification date.
     */
   public static final char VFS_MODIFICATION_DATE = (char)2;
   
   /** A value for use when calling get/setVFSFileDate in order to specify
     * that you wish to request the files last acccess date.
     */
   public static final char VFS_LAST_ACCESSED_DATE = (char)3;

// ==========================================================================

/** Creates a new DLP date object using the current date and time.
  */
public DLP_Date() {
   GregorianCalendar gregoriancalendar = new GregorianCalendar();
   gregoriancalendar.setTime(new Date());
   year = (char)gregoriancalendar.get(Calendar.YEAR);
   month = (byte)(gregoriancalendar.get(Calendar.MONTH)+1);
   day = (byte)gregoriancalendar.get(Calendar.DAY_OF_MONTH);
   hour = (byte)gregoriancalendar.get(Calendar.HOUR);
   if(hour == 24) hour = 0;
   minute = (byte)gregoriancalendar.get(Calendar.MINUTE);
   second = (byte)gregoriancalendar.get(Calendar.SECOND);
} // end-constructor

// --------------------------------------------------------------------------

/** Create a new DLP date object using the specified date and time
  * @param calendar the date and time you want to set this date object to.
  * @exception InvalidDLPDateException thrown if the provided calendar contains invalid information, or is null.
  */
public DLP_Date(Calendar calendar) throws InvalidDLPDateException {
   if(calendar == null) throw new InvalidDLPDateException("Calendar object is null.");
   year = (char)calendar.get(1);
   month = (byte)(calendar.get(2) + 1);
   day = (byte)calendar.get(5);
   hour = (byte)calendar.get(11);
   if(hour == 24) hour = 0;
   minute = (byte)calendar.get(12);
   second = (byte)calendar.get(13);
} // end-constructor

// --------------------------------------------------------------------------

/** Create a new DLP date object with the specified DLP formatted date bytes.
  * @param data the date and time data you want to set this date object to.
  * @exception InvalidDLPDateException thrown if the provided data contains invalid information, or is null.
  */
public DLP_Date(byte data[]) throws InvalidDLPDateException {
   if(data.length != 8) throw new InvalidDLPDateException("This is not a date array!");
   year = UnsignedByte.unsignedBytes2Char(data[0], data[1]);

   if(year < '\u076C') {
      year = 0;
      month = 0;
      day = 0;
      hour = 0;
      minute = 0;
      second = 0;
      return;
   }

   month = data[2];
   day = data[3];
   hour = data[4];
   minute = data[5];
   second = data[6];

   if(((((((((((year < '\u076C') | (month < 1)) | (month > 12)) | (day < 1)) | (day > 31)) | (hour < 0)) | (hour > 23)) | (minute < 0)) | (minute > 59)) | (second < 0)) | (second > 59)) {
      throw new InvalidDLPDateException("Invalid date array values detected!");
   } else {
      return;
   }
} // end-constructor

// ==========================================================================

/** Converts the provided Calendar object to a DLP two-byte Date representation.
  * @param calendar the date to convert.
  * @return a char representing the date portion of the provided calendar object.
  */
public static char calendar2DateType(Calendar calendar) {
   int i = 0;
   int j = calendar.get(1) - 1904;
   int k = calendar.get(2) + 1;
   int l = calendar.get(5);
   i += j * 512;
   i += k * 32;
   i += l;
   return (char)i;
} // end-method

// --------------------------------------------------------------------------

/** Convert this date/time object to a series of DLP date/time bytes.
  * @return a series of bytes representing this date object.
  */
public byte[] convertToBytes() {
   byte data[] = new byte[8];
   data[0] = (byte)(year >> 8);
   data[1] = (byte)(year & 0x00FF);
   data[2] = month;
   data[3] = day;
   data[4] = hour;
   data[5] = minute;
   data[6] = second;
   data[7] = 0;
   return data;
} // end-method

// --------------------------------------------------------------------------

/** Converts this date object to a Calendar object.
  * The current implementation will actually create a GregorianCalendar object,
  * and will return it as a Calendar object.
  * @return this date object represented as a Calendar object.
  */
public Calendar convertToCalendar() {
   GregorianCalendar gregoriancalendar = new GregorianCalendar();
   gregoriancalendar.set(year, month - 1, day, hour, minute, second);
   return gregoriancalendar;
} // end-method

// --------------------------------------------------------------------------

/** Convert this date object to the number of seconds since Jan 1st, 1904 @ 0000.
  * @return the  number of seconds since Jan 1st, 1904 @ 0000.
  */
public long convertToSeconds() {
   long currentTime;
   Calendar calendar;

   if (year==0) return 0;

   calendar = convertToCalendar();
   currentTime = calendar.getTime().getTime();

   return (long)((currentTime - JAN1_1904_MS_FROM_EPOCH)/1000L);
} // end-method

// --------------------------------------------------------------------------

/** Convert the provided date byte array into a Calendar object.
  * @param data an array containing DLP date information.
  * @param i the offset into the array where the date information starts.
  * @return the provided date byte array as a Calendar object.
  */
public static Calendar dateType2Calendar(byte data[], int i) {
   char c = UnsignedByte.unsignedBytes2Char(data[i], data[i + 1]);
   int j = 1904;
   int k = -1;
   j += (c & 0xfe00) / 512;
   k += (c & 0x1e0) / 32;
   int l = c & 0x1f;
   return new GregorianCalendar(j, k, l);
} // end-method

// --------------------------------------------------------------------------

/** Convert a value representing seconds since Jan 1st 1904 @ 0000 to a Calendar object.
  * @param i a value representing seconds since Jan 1st 1904 @ 0000.
  * @return a Calendar object containing the date and time represented by the input.
  */
public static Calendar seconds2Calendar(long i) {
   GregorianCalendar ret = new GregorianCalendar();
   Date date = new Date((i * 1000L) + JAN1_1904_MS_FROM_EPOCH);
   ret.setTime(date);
   return ret;
} // end-method

// --------------------------------------------------------------------------

/** Retreive this object as human-readable text.
  * @return  this object as human-readable text.
  */
    @Override
public String toString() {
   if(year != 0) {
      return convertToCalendar().getTime().toString();
   } else {
      return "null";
   }
} // end-method

// --------------------------------------------------------------------------

/** Creates a new empty DLP Date object.
  * This constructor will create a DLP date with year (and all other fields) set to 0.
  * @return a new empty DLP Date object.
  */
public static DLP_Date emptyDate() {
   DLP_Date d = new DLP_Date();
   d.year = 0;
   d.month = 0;
   d.day = 0;
   d.hour = 0;
   d.minute = 0;
   d.second = 0;
   return d;
} // end-static-method

// --------------------------------------------------------------------------

/** Converts a calendar object to a long representing seconds since January 1st, 1904 @ 00:00.
  * @param calendar a java.util.Calendar instance containing the time you want to convert.
  * @return a long representing seconds since January 1st, 1904 @ 00:00.
  */
public static long calendar2Seconds(Calendar calendar) {
   long currentTime = calendar.getTime().getTime();
   return (long)((currentTime - JAN1_1904_MS_FROM_EPOCH)/1000L);
} // end-method

// ==========================================================================

} // end-class
