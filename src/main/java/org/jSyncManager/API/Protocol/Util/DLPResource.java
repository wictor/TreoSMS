// --------------------------------------------------------------------------
// The jSyncManager Project -- Source File.
// Copyright (c) 1998 - 2004 Brad BARCLAY <bbarclay@jsyncmanager.org>
// --------------------------------------------------------------------------
// OSI Certified Open Source Software
// --------------------------------------------------------------------------
//
// This library is free software; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as published 
// by the Free Software Foundation; either version 2.1 of the License, or 
// (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of 
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public 
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// --------------------------------------------------------------------------
// $Id: DLPResource.java,v 3.3 2004/08/03 02:35:10 yaztromo Exp $
// --------------------------------------------------------------------------

package org.jSyncManager.API.Protocol.Util;

import org.jSyncManager.API.Protocol.DLPFunctionCallException;
import org.jSyncManager.API.Tools.UnsignedByte;

import java.io.Serializable;

// ==========================================================================

/** A class to store a handheld database resource.
  * This class holds a single data resource.
  * @author Brad BARCLAY &lt;bbarclay@jsyncmanager.org&gt;
  * <br>Last modified by: $Author: yaztromo $ on $Date: 2004/08/03 02:35:10 $.
  * @version $Revision: 3.3 $
  */
public class DLPResource implements Serializable {
   /** A field to hold the resource type value.                            */
   private int resourceType=0;

   /** A field to hold the resource ID for this resource.                  */
   private char resourceID=0;

   /** A field to hold the index value for this resource.                  */
   private char index=0;

   /** A field to store the size of this resource.                         */
   private char resourceSize=0;

   /** A field to store the data contained within this resource.           */
   private byte data[]=null;

   /** A flag to denote that all resources should be deleted during a delete operation.
     * Specifying this flag during a call to JHotSync.deleteResource will cause all 
     * resources in the specified database to be deleted.
     * @see org.jSyncManager.API.Protocol.JHotSync#deleteResource
     */
   public static final byte DELETE_ALL = (byte)0x80;

   private static final long serialVersionUID = 7713074292968936350L;

// ==========================================================================

/** Construct a new Resource object with null/zeroed values.
  */
DLPResource() {
} // end-constructor

// --------------------------------------------------------------------------

/** Create a new Resource object based on the provided resource byte array.
  * @param inData a byte array from the handheld representing a resource.
  * @exception DLPFunctionCallException thrown if there are any parsing errors.
  */
public DLPResource(byte inData[]) throws DLPFunctionCallException {
   int i = 0;

   try {
      setResourceType(UnsignedByte.unsignedBytes2Int(inData[i++], inData[i++], inData[i++], inData[i++]));
      setResourceID(UnsignedByte.unsignedBytes2Char(inData[i++], inData[i++]));
      setIndex(UnsignedByte.unsignedBytes2Char(inData[i++], inData[i++]));
      setResourceSize(UnsignedByte.unsignedBytes2Char(inData[i++], inData[i++]));
      data = new byte[getResourceSize()];
      System.arraycopy(inData, i, data, 0, inData.length-i);
   } catch(Throwable throwable) {
      throwable.printStackTrace(System.err);
      System.err.println("=======================================================================");
      throw new DLPFunctionCallException(throwable.toString(), DLPFunctionCallException.NO_DLP_ERROR);
   }
} // end-constructor

// ==========================================================================

/** Retreives the data portion of the resource.
  * @return  the data portion of the resource.
  */
public byte[] getData() {
   return data;
} // end-method

// --------------------------------------------------------------------------

/** Retreives the index of the resource.
  * @return  the index of the resource.
  */
public char getIndex() {
   return index;
} // end-method

// --------------------------------------------------------------------------

/** Retreives the ID number associated with this resource.
  * @return  the ID number associated with this resource.
  */
public char getResourceID() {
   return resourceID;
} // end-method

// --------------------------------------------------------------------------

/** Retreives the size of this resource.
  * @return  the size of this resource.
  */
public char getResourceSize() {
   return resourceSize;
} // end-method

// --------------------------------------------------------------------------

/** Retreives the type of this resource.
  * @return the type of this resource.
  */
public int getResourceType() {
   return resourceType;
} // end-method

// --------------------------------------------------------------------------

/** Retreives the String representing this resources type value.
  * @return  the String representing this resources type value.
  */
public String getResourceTypeString() {
   byte temp[] = new byte[4];
   UnsignedByte.copyIntToByteArray(resourceType, temp, 0);
   return new String(temp);
} // end-method

// --------------------------------------------------------------------------

/** Sets the data array for this resource.
  * @param data the data array for this resource.
  */
public void setData(byte data[]) {
   this.data = data;
} // end-method

// --------------------------------------------------------------------------

/** Sets the index value to be associated with this resource.
  * If you're creating your own Resource object from scratch, you can set
  * this value to 0, as the handheld will normally assign this value.
  * @param i the index value for this resource.
  */
public void setIndex(char i) {
   index = i;
} // end-method

// --------------------------------------------------------------------------

/** Sets the resource ID to be associated with this resource.
  * If you're creating your own Resource object from scratch, you can set
  * this value to 0, as the handheld will normally assign this value.
  * @param id the resource ID number for this resource.
  */
public void setResourceID(char id) {
   resourceID = id;
} // end-method

// --------------------------------------------------------------------------

/** Sets the size of this resource.
  * @param size the size of this resource.
  */
public void setResourceSize(char size) {
   resourceSize = size;
} // end-method

// --------------------------------------------------------------------------

/** Sets this resources type.
  * @param i this resources type.
  */
public void setResourceType(int i) {
   resourceType = i;
} // end-method

// ==========================================================================

} // end-class

