/*
 * Project: TreoSMS 2008-01-22 - https://gitlab.com/wictor/TreoSMS
 * Author: wictor@sysrq.cz
 * Licensed under the [GPL](http://www.gnu.org/licenses/gpl.html).
 */

package cz.sysrq.treosms.data;

import org.junit.BeforeClass;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import static org.junit.Assert.*;

/**
 * {@link SmsDate} unit test, time zone testing.
 *
 * @author wictor
 * @since 31.12.2016
 */
public class SmsDateTest {

    @BeforeClass
    public static void before() {
        // arrange TZ consistent environment
        System.setProperty("user.timezone", "CET");
    }

    @Test
    public void testGetFormattedDate() {
        SmsDate smsDate = new SmsDate(3254652884L, SmsDateType.T8000);

        String formattedDate = smsDate.getFormattedDate(0);

        assertEquals("2007-02-18 14:14:44", formattedDate);
    }

    @Test
    public void testGetFormattedDateWithFormat() {
        SmsDate smsDate = new SmsDate(3254652884L, SmsDateType.T8000);

        String formattedDate = smsDate.getFormattedDate(3600, "dd.MM.yyyy HH:mm:ss z");

        assertEquals("18.02.2007 15:14:44 CET", formattedDate);
    }

    @Test
    public void testGetFormattedDateWithTimeZone() {
        SmsDate smsDate = new SmsDate(3254652884L, SmsDateType.T8000);

        String formattedDate = smsDate.getFormattedDate(3600, "dd.MM.yyyy HH:mm:ss z", TimeZone.getTimeZone("UTC"));

        assertEquals("18.02.2007 14:14:44 UTC", formattedDate);
    }

    @Test
    public void testFormattingDateWithTimeZone() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss z");
        long time = 1483202000000L; // Sat, 31 Dec 2016 16:33:20 GMT
        Date date = new Date(time);

        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        assertEquals("31.12.2016 16:33:20 GMT", sdf.format(date));

        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        assertEquals("31.12.2016 16:33:20 UTC", sdf.format(date));

        sdf.setTimeZone(TimeZone.getTimeZone("CET"));
        assertEquals("31.12.2016 17:33:20 CET", sdf.format(date));
    }
}