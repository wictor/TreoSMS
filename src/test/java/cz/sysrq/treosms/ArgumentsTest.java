/*
 * Project: TreoSMS 2008-01-22 - https://gitlab.com/wictor/TreoSMS
 * Author: wictor@sysrq.cz
 * Licensed under the [GPL](http://www.gnu.org/licenses/gpl.html).
 */

package cz.sysrq.treosms;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.junit.Assert;
import org.junit.Test;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Application arguments unit test.
 * <p/>
 *
 * @author wictor
 */
public class ArgumentsTest {

    @Test
    public void testParsingArguments() throws ParseException {
        // arrange:
        String[] args = {"-t", "--input", "MessagesDatabase.PDB"};

        Options options = Main.createApplicationOptions();

        // act:
        CommandLine commandLine;
        CommandLineParser parser = new DefaultParser();
        commandLine = parser.parse(options, args);

        // assert:
        Assert.assertTrue(commandLine.hasOption("t"));
        Assert.assertEquals("MessagesDatabase.PDB", commandLine.getOptionValue("i"));
        Assert.assertNull(commandLine.getOptionValue("o"));
        Assert.assertEquals("0", commandLine.getOptionValue("z", "0"));
    }

    @Test
    public void testParsingArgumentsModeDefault() throws ParseException {
        // arrange:
        String[] args = {"-i", "MessagesDatabase.PDB"};

        Options options = Main.createApplicationOptions();

        // act:
        CommandLine commandLine;
        CommandLineParser parser = new DefaultParser();
        commandLine = parser.parse(options, args);

        // assert:
        Assert.assertEquals("MessagesDatabase.PDB", commandLine.getOptionValue("i"));
        Assert.assertNull(commandLine.getOptionValue("o"));
        Assert.assertEquals("0", commandLine.getOptionValue("z", "0"));

        OptionGroup group = options.getOptionGroup(options.getOption("t"));
        Assert.assertNotNull(group);
        Assert.assertNull(group.getSelected());
    }

    @Test
    public void testParsingArgumentsModeRecords() throws ParseException {
        // arrange:
        String[] args = {"-i", "MessagesDatabase.PDB", "-r"};

        Options options = Main.createApplicationOptions();

        // act:
        CommandLine commandLine;
        CommandLineParser parser = new DefaultParser();
        commandLine = parser.parse(options, args);

        // assert:
        Assert.assertEquals("MessagesDatabase.PDB", commandLine.getOptionValue("i"));
        Assert.assertNull(commandLine.getOptionValue("o"));
        Assert.assertEquals("0", commandLine.getOptionValue("z", "0"));

        OptionGroup group = options.getOptionGroup(options.getOption("t"));
        Assert.assertNotNull(group);
        Assert.assertNotNull(group.getSelected());
        Assert.assertEquals("r", group.getSelected());
    }

    @Test
    public void testHelp() throws ParseException {
        // arrange:
        Options options = Main.createApplicationOptions();
        HelpFormatter formatter = new HelpFormatter();

        // act
        final String EOL = System.getProperty("line.separator");
        StringWriter out = new StringWriter();
        formatter.printHelp(new PrintWriter(out), HelpFormatter.DEFAULT_WIDTH, "TreoSMS", Main.usageHdr, options,
                HelpFormatter.DEFAULT_LEFT_PAD, HelpFormatter.DEFAULT_DESC_PAD, Main.usageFtr, true);

        // assert
        Assert.assertEquals("usage: TreoSMS -i <arg> [-l | -r | -t] [-o <arg>]   [-z <arg>]" + EOL +
                        "Reads Palm Treo 680 file Messages_Database.PDB and converts to selected" + EOL +
                        "output." + EOL +
                        " -i,--input <arg>      specify input PDB file" + EOL +
                        " -l,--list             list output format: show summary information about" + EOL +
                        "                       input content (default)" + EOL +
                        " -o,--output <arg>     specify output file instead of stdout (default is" + EOL +
                        "                       stdout - console)" + EOL +
                        " -r,--records          debug output format: dumps all records from input" + EOL +
                        " -t,--csv              CSV output format: convert the messages to the CSV" + EOL +
                        "                       format" + EOL +
                        " -z,--timediff <arg>   specify timezone difference in hours (default = 0," + EOL +
                        "                       GMT+0)" + EOL + EOL +
                        "EXAMPLE:" + EOL +
                        " 1. To convert your messages to CSV (this is what you probably want) using" + EOL +
                        "zone GMT+1:" + EOL +
                        "   java -jar \"TreoSMS.jar\" -z +1 -t -i Messages_Database.PDB -o sms.csv" + EOL + EOL +
                        "https://gitlab.com/wictor/TreoSMS" + EOL +
                        "wictor@sysrq.cz" + EOL,
                out.toString());
    }
}
