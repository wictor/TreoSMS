/*
 * Project: TreoSMS 2008-01-22 - https://gitlab.com/wictor/TreoSMS
 * Author: wictor@sysrq.cz
 * Licensed under the [GPL](http://www.gnu.org/licenses/gpl.html).
 */

package cz.sysrq.treosms.parser;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Coder from binary to ascii, like Base64.
 *
 * @author wictor
 */

public class AsciiEncoder {

    private static final int CHARS_PER_BYTE = 2;

    public static String encode(byte[] bytes) {
        String format = "%1$0" + CHARS_PER_BYTE + "x";
        StringBuilder sb = new StringBuilder(bytes.length * CHARS_PER_BYTE);
        for (byte b : bytes) {
            String str = String.format(format, b);
            sb.append(str);
        }
        return sb.toString();
    }

    public static byte[] decode(String string) {
        int size = string.length() / CHARS_PER_BYTE;
        byte[] bytes = new byte[size];
        for (int i = 0; i < size; i++) {
            int start = i * CHARS_PER_BYTE;
            int end = (i + 1) * CHARS_PER_BYTE;
            String cut = string.substring(start, end);
            int n = Integer.parseInt(cut, 16);
            byte b = (byte) n;
            bytes[i] = b;
        }
        return bytes;
    }

    @Test
    public void testEncode() {
        byte[] code = {0x40, 0x09};
        byte[] data = {0x10, 0x10, 0x11, 0x12};

        String s = AsciiEncoder.encode(concat(code, data));

        assertEquals("400910101112", s);
    }

    @Test
    public void testDecode() {
        String input = "40090025000CD3930323036323";
        // 9020620

        byte[] buf = AsciiEncoder.decode(input);

        int[] expected = {0x40, 0x09, 0x00, 0x25, 0x00, 0x0C, 0xD3, 0x93, 0x03, 0x23, 0x03, 0x63, 0x23};

        assertEquals(expected.length, buf.length);
        for (int i = 0; i < buf.length; i++) {
            assertEquals(expected[i], buf[i] & 0xff);
        }
    }

    private static byte[] concat(byte[] a, byte[] b) {
        int aLen = a.length;
        int bLen = b.length;
        byte[] c = new byte[aLen + bLen];
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);
        return c;
    }

}
