/*
 * Project: TreoSMS 2008-01-22 - https://gitlab.com/wictor/TreoSMS
 * Author: wictor@sysrq.cz
 * Licensed under the [GPL](http://www.gnu.org/licenses/gpl.html).
 */

package cz.sysrq.treosms.parser;

import cz.sysrq.treosms.data.Dir;
import cz.sysrq.treosms.data.Message;
import cz.sysrq.treosms.data.MessageParsingException;
import cz.sysrq.treosms.data.ParsingResult;
import cz.sysrq.treosms.data.SmsDateType;
import org.jSyncManager.API.Protocol.Util.DLPDatabase;
import org.jSyncManager.API.Protocol.Util.DLPRecord;
import org.jSyncManager.API.Protocol.Util.ParseException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

/**
 * {@link Treo680MsgParser} unit test.
 *
 * (Anonymized data - one can test own PDB file using the last prepared method)
 */
public class Treo680MsgParserTest {

    private Treo680MsgParser parser;

    @BeforeClass
    public static void before() {
        // arrange TZ consistent environment
        System.setProperty("user.timezone", "CET");
    }

    @Before
    public void setup() {
        parser = new Treo680MsgParser();
    }

    @Test
    public void testMsg0c() throws MessageParsingException, ParseException {
        String m = "00000000000100000002400C0021000000000000000000000000000000000000000036303800000000000000000000200200" +
                "6B44656B756A656D652E205072656A657465207369207A6D656E6974207461726966206E61204E616269746F203130302E20" +
                "506F20767972697A656E692076616D2064616D6520766564657420746578746F766B6F752E2048657A6B792064656E2C2056" +
                "6F6461666F6E65008000C1FE0FD40005C1FE0FD40006C1FE100A000200000001000F00000000000D00000000000E00000002" +
                "000000000003001B5472736D00030000016D4000000400F211D0";

        DLPRecord record = new DLPRecord();
        record.setData(AsciiEncoder.decode(m));
        Message message = parser.parseRecord(record);

        assertNotNull(message);
        assertEquals(Dir.RECEIVED, message.getDirection());
        assertEquals("Dekujeme. Prejete si zmenit tarif na Nabito 100. Po vyrizeni vam dame vedet textovkou. Hezky den, Vodafone", message.getText());
        assertNotNull(message.getParty());
        assertEquals("608", message.getParty().getNumber());
        assertNull(message.getParty().getName());
        assertEquals("2007-02-18 14:14:44", message.getDatetime(SmsDateType.T8000, 0));
        assertEquals("2007-02-18 14:14:44", message.getDatetime(SmsDateType.T0005, 0));
        assertEquals("2007-02-18 14:15:38", message.getDatetime(SmsDateType.T0006, 0));
    }

    @Test
    public void testMsg02() throws MessageParsingException, ParseException {
        String m = "00000000000100000002000200400011000000000003000F000000008000C31B08E20007C31B08E20006C31B08E24009" +
                "003C00000000000000000000000000000000000000332B343230373735313131313131004A6F73656620284D29000000" +
                "0000000000000000000000000000001B5472736D200200445A6461722C20756A656C6F206E616D206D6574726F2E2054" +
                "616B7A6520617369202B31306D696E2E204F6D6C757674652070726F73696D207A706F7A64656E692E2E2E0000030000" +
                "014E4000000800F2132500000002";

        DLPRecord record = new DLPRecord();
        record.setData(AsciiEncoder.decode(m));
        Message message = parser.parseRecord(record);

        assertNotNull(message);
        assertEquals(Dir.SENT, message.getDirection());
        assertEquals("Zdar, ujelo nam metro. Takze asi +10min. Omluvte prosim zpozdeni...", message.getText());
        assertEquals("+420775111111", message.getParty().getNumber());
        assertEquals("Josef (M)", message.getParty().getName());
        assertEquals("2007-09-22 19:01:06", message.getDatetime(SmsDateType.T8000, 0));
        assertEquals("2007-09-22 19:01:06", message.getDatetime(SmsDateType.T0006, 0));
        assertEquals("2007-09-22 19:01:06", message.getDatetime(SmsDateType.T0007, 0));
    }

    @Test
    public void testMsg002002() throws MessageParsingException, ParseException {
        String m = "00000000000100000002000000000003001B5472736D0002004000112002002F4175746F206F6465767A646176616D65" +
                "20646C6520706C616E752076652031346820762044656A7669636963682E008000C2EEEEC10007C2EEEEC10006C2EEEE" +
                "C10008000000004009003900000000000000000000000000000000000000C836303136303236303300566C6164696DED" +
                "7220284D29000000000000000000000F0000000000030000014E4000000400000002";

        DLPRecord record = new DLPRecord();
        record.setData(AsciiEncoder.decode(m));
        Message message = parser.parseRecord(record);

        assertNotNull(message);
        assertEquals(Dir.SENT, message.getDirection());
        assertEquals("Auto odevzdavame dle planu ve 14h v Dejvicich.", message.getText());
        assertEquals("601602603", message.getParty().getNumber());
        assertEquals("Vladimír (M)", message.getParty().getName());
        assertEquals("2007-08-20 08:09:53", message.getDatetime(SmsDateType.T8000, 0));
        assertEquals("2007-08-20 08:09:53", message.getDatetime(SmsDateType.T0006, 0));
        assertEquals("2007-08-20 08:09:53", message.getDatetime(SmsDateType.T0007, 0));
    }

    @Test
    public void testMsg008000() throws MessageParsingException, ParseException {
        String m = "00000000000100000002000000000003001B5472736D0002004000118000C391375E0007C391375E0006C391375E0008" +
                "000000004009002500000000000000000000000000000000000000CD3930323036323000000000000000000000000F00" +
                "000000200200044450540000030000014E400000080000000200F21E6C";

        DLPRecord record = new DLPRecord();
        record.setData(AsciiEncoder.decode(m));
        Message message = parser.parseRecord(record);

        assertNotNull(message);
        assertEquals(Dir.SENT, message.getDirection());
        assertEquals("DPT", message.getText());
        assertEquals("9020620", message.getParty().getNumber());
        assertNull(message.getParty().getName());
        assertEquals("2007-12-21 09:26:54", message.getDatetime(SmsDateType.T8000, 0));
        assertEquals("2007-12-21 09:26:54", message.getDatetime(SmsDateType.T0006, 0));
        assertEquals("2007-12-21 09:26:54", message.getDatetime(SmsDateType.T0007, 0));
    }

    @Test @Ignore
    public void testParseDb() throws Exception {
        // sample test of the whole PDB file (data omitted for public release)
        File file = new File("resources/pdb/Messages_Database.PDB");
        assertNotNull(file);
        assertTrue(file.exists());
        assertEquals(81097, file.length());

        DLPDatabase db = DLPDatabase.importFromFile(file);
        assertNotNull(db);

        ParsingResult result = parser.parseDb(db);
        assertNotNull(result);
        assertEquals(0, result.getFailedRecords().size());
        assertEquals(39, result.getSkippedRecords().size());
        assertEquals(286, result.getMessages().size());
    }

}