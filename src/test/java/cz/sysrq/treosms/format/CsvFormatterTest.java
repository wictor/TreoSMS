/*
 * Project: TreoSMS 2008-01-22 - https://gitlab.com/wictor/TreoSMS
 * Author: wictor@sysrq.cz
 * Licensed under the [GPL](http://www.gnu.org/licenses/gpl.html).
 */

package cz.sysrq.treosms.format;

import cz.sysrq.treosms.data.Dir;
import cz.sysrq.treosms.data.Message;
import cz.sysrq.treosms.data.Party;
import cz.sysrq.treosms.data.SmsDate;
import cz.sysrq.treosms.data.SmsDateType;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * {@link CsvFormatter} unit test.
 */
public class CsvFormatterTest {

    private CsvFormatter formatter;

    @BeforeClass
    public static void before() {
        // arrange TZ consistent environment
        System.setProperty("user.timezone", "CET");
    }

    @Before
    public void setup() {
        formatter = new CsvFormatter(0);
    }

    @Test
    public void testFormat() throws Exception {
        // arrange
        SmsDate smsDate = new SmsDate(3254652884L, SmsDateType.T8000);
        Party party = new Party("+123456", "Katy");
        Message msg = new Message(Collections.singletonList(smsDate), party, Dir.SENT, "Message text");

        // act
        String str = formatter.format(msg);

        // assert
        assertNotNull(str);
        assertEquals("SENT,\"Katy\",+123456,2007-02-18 14:14:44,\"Message text\"", str);
    }
}